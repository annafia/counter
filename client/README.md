# client
# There are some screenshots of this apps in folder "screenshots"

<!--- Administrator Account -->
username: m.annafia
password: september09
encryption password: bcrypt
authorization token: jwt

<!------------------- How to setup client -------------------------->
<!--- Needs: -->
1. Internet Connection
2. Web Server (I use apache)

<!--- Steps: -->
1. Clone this project from Gitlab
2. Setup Web Server using alias to beautify the url
<!--
    Alias	/dealer-managements		C:\xampp5\htdocs\dumbways\dealer-managements\client
    <Directory "C:\xampp5\htdocs\dumbways\dealer-managements\client">
        Require all granted

        RemoveOutputFilter js
        RemoveOutputFilter css

        RewriteEngine On

        RewriteCond %{HTTP:Accept-Encoding} br
        RewriteCond %{REQUEST_FILENAME}.br -f
        RewriteRule ^(.*)$ $1.br [L]
    </Directory>
-->
3. Don't forget to restart your web server after you make some changes