let cars = {
    list: {
        init() {
            $.get(`${VAR.baseUrl}cars`).done(function(res) {
                if(res.status) {
                    res = res.data
                    eaControl.render(`app/cars/cars.list`, res, '#eaContent', function () {
                        $('#table-list-cars').DataTable()
                        $('[name="brand_id"]').selectpicker()
                        cars.list.event(res)
                    })
                } else {
                    eaControl.notif('success', `<h5 class="title">Error !</h5> <h6 class="message">${res.message}</h6>`)
                }
            })
        },
        event(res) {
            $('#table-list-cars').on('click', '[aria-action="delete-cars"]', function() {
                let cars_id = $(this).data('id')
                eaControl.notif('confirm', '<h5 class="title">Warning !</h5>'+
                    '<h6 class="message">Are you sure you want to delete this cars ?</h6>', function() {
                    /** --------------- Do action here --------------- */
                    $.ajax({ url: `${VAR.baseUrl}cars/${cars_id}`, method: 'DELETE' }).done(function(res) {
                        if (res.status) {
                            cars.list.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Cars was successfully deleted.</h6>')
                        } else {
                            eaControl.notif('error', '<h5 class="title">Error !</h5><h6 class="message">'+res.message+'</h6>')
                        }
                    })
                })
            })
    
            const carsValidator = $('#form-cars').validate({
                rules       : { "brand_name": "required" },
                messages    : { "brand_name": "Name is required" },
                invalidHandler: function (event, carsValidator) {
                    const errors = carsValidator.numberOfInvalids()
                    if (errors) {
                        $('#form-cars-card-error .alert').fadeIn("fast")
                    }
                },
                errorContainer: '#form-cars-card-error .alert',
                errorLabelContainer: '#formCarsErrorMessage',
                wrapper: 'span',
                submitHandler: function (form) {
                    /** Form cars */
                    const formData = new FormData($(form)[0]);
                    $('[aria-action="submit-cars"]').attr('disabled', true)
                    $.ajax({ url: `${VAR.baseUrl}cars`, method: 'POST', processData: false, contentType: false, data: formData }).done(function(res) {
                        if (res.status) {
                            cars.list.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Cars was successfully added.</h6>')
                        } else {
                            carsValidator.showErrors({'brand_name': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-cars"]').attr('disabled', false)
                    })
                    return false
                }
            })
        }
    },
    view: {
        init(carsid) {
            $.get(`${VAR.baseUrl}cars/${carsid}`).done(function(res) {
                if(res.status) {
                    res = res.data
                    res.baseUrl = VAR.baseUrl
                    res.token = document.cookie
                    eaControl.render(`app/cars/cars.view`, res, '#eaContent', function () {
                        $('[name="brand_id"]').selectpicker()
                        cars.view.event(res)
                    })
                } else {
                    eaControl.notif('success', `<h5 class="title">Error !</h5> <h6 class="message">${res.message}</h6>`)
                }
            })
        },
        event(res) {

        }
    }
}