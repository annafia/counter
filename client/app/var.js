const VAR = {
	appName 			: 'counter',
	brand 				: 'Counter Management',
	eaUrl 				: '/counter-api/',
	baseUrl 			: '/counter-api/',
	month				: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
	fullMonth			: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
	isUserInitEnabled	: true,
    headerTemplate  	: './app/other/header',
    contentTemplate 	: '/core/template/content',
}

if (typeof require !== 'undefined') {
	require.config({
		paths: {
			jquery			    : '../vendor/jquery/jquery-3.3.1.min',
			popper			    : '../vendor/bootstrap/4.3.1/popper.min',
			modernizr		    : '../vendor/modernizr/modernizr-custom',
			sammy			    : '../vendor/sammy/sammy-0.7.5.min',
			dust			    : '../vendor/dust/dust-full-2.7.2.min',
			dust_helper	    	: '../vendor/dust/dust-helpers-1.1.1.min',
			bootstrap		    : '../vendor/bootstrap/4.3.1/bootstrap.bundle.min',
			datatables			: '../vendor/core/xplugins/jquery.dataTables.min',
			dtbootstrap			: '../vendor/core/xplugins/dataTables.bootstrap4.min',
			bootstrap_select	: 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.11/js/bootstrap-select.min',
			select2				: '../vendor/core/xplugins/select2.min',
			jquery_validate		: '../vendor/jquery-validate/1.19.1/jquery.validate.min',
			jv_additional_method: "../vendor/jquery-validate/1.19.1/additional-methods.min",
			overlayscrollbars	: '../vendor/overlayscrollbars/OverlayScrollbars.min',
            ea				    : '../vendor/core/js/core',
			app				    : 'app',
			
			"app.dashboard.admin"		: 'dashboard/dashboard.admin',
			"app.dashboard.personal"	: 'dashboard/dashboard.personal',
			"app.customer"		: 'customer/customer',
			"app.transaction"	: 'transaction/transaction',
			"app.packages"		: 'packages/packages',
			"app.provider"		: 'provider/provider',
			"app.account"		: 'account/account',
			"app.settings"		: 'settings/settings',
		},
		shim: { /*dependencies*/
			bootstrap		    : ['jquery'],
			sammy			    : {
				deps	: ['jquery'],
				exports	: 'Sammy'
			},
			ea				    : ['modernizr', 'jquery', 'sammy', 'bootstrap', 'dust', 'dust_helper'],
			eachat			    : ['ea'],
			dust_helper		    : ['dust'],
			bsselect		    : ['jquery', 'bootstrap'],
			jqueryform		    : ['jquery'],
			dtselect		    : ['datatables'],
            dtbootstrap		    : ['datatables'],
			app				    : ['ea'],
		}
	})
	/*load scripts*/
	require(['app'])
	define(['jquery', 'sammy'], function($, Sammy) { $.sammy = Sammy })
}
