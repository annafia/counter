let provider = {
    init() {
        $.get(`${VAR.baseUrl}provider`).done(function(res) {
            if(res.status) {
                res = res.data
                eaControl.render(`app/provider/provider.list`, {provider: res}, '#eaContent', function () {
                    $('#table-list-provider').DataTable()
                    provider.event(res)
                })
            } else {
                eaControl.notif('success', `<h5 class="title">Error !</h5> <h6 class="message">${res.message}</h6>`)
            }
        })
    },
    event(res) {
        let editElement = null

        $('#table-list-provider').on('click', '[aria-action="delete-provider"]', function() {
            let provider_id = $(this).data('id')
            eaControl.notif('confirm', '<h5 class="title">Warning !</h5>'+
                '<h6 class="message">Are you sure you want to delete this provider ?</h6>', function() {
                /** --------------- Do action here --------------- */
                $.ajax({ url: `${VAR.baseUrl}provider/${provider_id}`, method: 'DELETE' }).done(function(res) {
                    if (res.status) {
                        provider.init()
                        eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Provider was successfully deleted.</h6>')
                    } else {
                        eaControl.notif('error', '<h5 class="title">Error !</h5><h6 class="message">'+res.message+'</h6>')
                    }
                })
            })
        })

        $('#table-list-provider').on('click', '[aria-action="edit-provider"]', function() {
            $('[container="form-subject"]').html('Edit')
            $('[aria-action="cancel-edit-provider"]').fadeIn()

            let provider_id = $(this).data('id')
            for (let i = 0; i < res.length; i++) {
                let row = res[i]
                if(row.id == provider_id) { editElement = row }
            }

            $('[name="provider_name"]').val(editElement.provider_name)
            $('[name="provider_name"]').focus()
        })

        $('[aria-action="cancel-edit-provider"]').click(function() {
            $('[container="form-subject"]').html('Add')
            $('[aria-action="cancel-edit-provider"]').fadeOut()
            $('[name="provider_name"]').val('')
            $('[name="provider_name"]').focus()
            editElement = null
        })

        const providerValidator = $('#form-provider').validate({
            rules       : { "provider_name": "required" },
            messages    : { "provider_name": "Name is required" },
            invalidHandler: function (event, providerValidator) {
                const errors = providerValidator.numberOfInvalids()
                if (errors) {
                    $('#form-provider-card-error .alert').fadeIn("fast")
                }
            },
            errorContainer: '#form-provider-card-error .alert',
            errorLabelContainer: '#formProviderErrorMessage',
            wrapper: 'span',
            submitHandler: function (form) {
                /** Form provider */
                const formData = $(form).serialize()
                $('[aria-action="submit-provider"]').attr('disabled', true)
                if(!editElement) {
                    $.ajax({ url: `${VAR.baseUrl}provider`, method: 'POST', data: formData }).done(function(res) {
                        if (res.status) {
                            provider.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Provider was successfully added.</h6>')
                        } else {
                            providerValidator.showErrors({'provider_name': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-provider"]').attr('disabled', false)
                    })
                } else {
                    $.ajax({ url: `${VAR.baseUrl}provider/${editElement.id}`, method: 'PUT', data: formData }).done(function(res) {
                        if (res.status) {
                            provider.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Provider was successfully edited.</h6>')
                        } else {
                            providerValidator.showErrors({'provider_name': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-provider"]').attr('disabled', false)
                    })
                }
                return false
            }
        })
    }
}