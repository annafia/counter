let transaction = {
    init() {
        $.get(`${VAR.baseUrl}transaction`).done(function(res) {
            if(res.status) {
                res = res.data
                eaControl.render(`app/transaction/transaction.list`, res, '#eaContent', function () {
                    $('#table-list-transaction').DataTable()
                    $('[name="package_id"]').selectpicker()
                    
                    $('[name="customer_id"]').select2({
                        placeholder: 'Search customer by name or phone ...',
                        minimumInputLength: 3,
                        ajax: {
                            url: `${VAR.baseUrl}transaction/find-customer`,
                            delay: 200,
                            data: function (query) { return query },
                            processResults: function (res) {
                                if(res.status) {
                                    customerResults = res.data
                                    return { results: res.data }
                                } else {
                                    eaControl.notif('success', `<h5 class="title">Error !</h5>
                                        <h6 class="message">${res.message}</h6>`)
                                    return { results: [] }
                                }
                            },
                        }
                    })

                    transaction.event(res)
                })
            } else {
                eaControl.notif('success', `<h5 class="title">Error !</h5> <h6 class="message">${res.message}</h6>`)
            }
        })
    },
    event(res) {
        let editElement = null

        $('#table-list-transaction').on('click', '[aria-action="delete-transaction"]', function() {
            let transaction_id = $(this).data('id')
            eaControl.notif('confirm', '<h5 class="title">Warning !</h5>'+
                '<h6 class="message">Are you sure you want to delete this transaction ?</h6>', function() {
                /** --------------- Do action here --------------- */
                $.ajax({ url: `${VAR.baseUrl}transaction/${transaction_id}`, method: 'DELETE' }).done(function(res) {
                    if (res.status) {
                        transaction.init()
                        eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Transaction was successfully deleted.</h6>')
                    } else {
                        eaControl.notif('error', '<h5 class="title">Error !</h5><h6 class="message">'+res.message+'</h6>')
                    }
                })
            })
        })

        // $('#table-list-transaction').on('click', '[aria-action="edit-transaction"]', function() {
        //     $('[container="form-subject"]').html('Edit')
        //     $('[aria-action="cancel-edit-transaction"]').fadeIn()

        //     let transaction_id = $(this).data('id')
        //     for (let i = 0; i < res.transaction.length; i++) {
        //         let row = res.transaction[i]
        //         if(row.id == transaction_id) { editElement = row }
        //     }
        //     $('[name="package_id"]').val(editElement.package_id)
        //     $('[name="package_id"]').selectpicker('refresh')

        //     $('[name="transaction_name"]').val(editElement.transaction_name)
        //     $('[name="purchase"]').val(editElement.purchase)
        //     $('[name="sell"]').val(editElement.sell)
        //     $('[name="transaction_name"]').focus()
        // })

        // $('[aria-action="cancel-edit-transaction"]').click(function() {
        //     $('[container="form-subject"]').html('Add')
        //     $('[aria-action="cancel-edit-transaction"]').fadeOut()
            
        //     $('[name="package_id"]').val('')
        //     $('[name="package_id"]').selectpicker('refresh')

        //     $('[name="transaction_name"]').val('')
        //     $('[name="purchase"]').val('')
        //     $('[name="sell"]').val('')
        //     $('[name="transaction_name"]').focus()
        //     editElement = null
        // })

        const transactionValidator = $('#form-transaction').validate({
            rules       : { 
                "customer_id": "required",
                "package_id": "required",
                "transaction_date": "required",
            },
            messages    : { 
                "customer_id": "Customer is required",
                "package_id": "Provider is required",
                "transaction_date": "Purchase price is required"
            },
            invalidHandler: function (event, transactionValidator) {
                const errors = transactionValidator.numberOfInvalids()
                if (errors) {
                    $('#form-transaction-card-error .alert').fadeIn("fast")
                }
            },
            errorContainer: '#form-transaction-card-error .alert',
            errorLabelContainer: '#formTransactionErrorMessage',
            wrapper: 'span',
            submitHandler: function (form) {
                /** Form transaction */
                const formData = $(form).serialize()
                $('[aria-action="submit-transaction"]').attr('disabled', true)
                if(!editElement) {
                    $.ajax({ url: `${VAR.baseUrl}transaction`, method: 'POST', data: formData }).done(function(res) {
                        if (res.status) {
                            transaction.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Transaction was successfully added.</h6>')
                        } else {
                            transactionValidator.showErrors({'transaction_date': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-transaction"]').attr('disabled', false)
                    })
                } else {
                    $.ajax({ url: `${VAR.baseUrl}transaction/${editElement.id}`, method: 'PUT', data: formData }).done(function(res) {
                        if (res.status) {
                            transaction.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Transaction was successfully edited.</h6>')
                        } else {
                            transactionValidator.showErrors({'transaction_date': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-transaction"]').attr('disabled', false)
                    })
                }
                return false
            }
        })
    }
}