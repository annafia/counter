let account = {
    list: {
        init() {
            $.get(`${VAR.baseUrl}account`).done(function(res) {
                if(res.status) {
                    res = res.data
                    eaControl.render(`app/account/account.list`, res, '#eaContent', function () {
                        $('#table-list-account').DataTable()
                        $('[name="roles"]').selectpicker()
                        account.list.event(res)
                    })
                } else {
                    eaControl.notif('success', `<h5 class="title">Error !</h5> <h6 class="message">${res.message}</h6>`)
                }
            })
        },
        event(res) {
            $('#table-list-account').on('click', '[aria-action="delete-account"]', function() {
                let account_id = $(this).data('id')
                eaControl.notif('confirm', '<h5 class="title">Warning !</h5>'+
                    '<h6 class="message">Are you sure you want to delete this account ?</h6>', function() {
                    /** --------------- Do action here --------------- */
                    $.ajax({ url: `${VAR.baseUrl}account/${account_id}`, method: 'DELETE' }).done(function(res) {
                        if (res.status) {
                            account.list.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Account was successfully deleted.</h6>')
                        } else {
                            eaControl.notif('error', '<h5 class="title">Error !</h5><h6 class="message">'+res.message+'</h6>')
                        }
                    })
                })
            })
    
            const accountValidator = $('#form-account').validate({
                rules       : { 
                    "name": "required",
                    "roles": "required",
                    "username": "required",
                    "password": "required",
                    "repassword": "required",
                },
                messages    : { 
                    "name": "Name is required",
                    "roles": "Roles is required",
                    "username": "Username is required",
                    "password": "Password is required",
                    "repassword": "Repassword is required",
                },
                invalidHandler: function (event, accountValidator) {
                    const errors = accountValidator.numberOfInvalids()
                    if (errors) {
                        $('#form-account-card-error .alert').fadeIn("fast")
                    }
                },
                errorContainer: '#form-account-card-error .alert',
                errorLabelContainer: '#formAccountErrorMessage',
                wrapper: 'span',
                submitHandler: function (form) {
                    /** Form account */
                    const formData = new FormData($(form)[0]);
                    $('[aria-action="submit-account"]').attr('disabled', true)
                    $.ajax({ url: `${VAR.baseUrl}account`, method: 'POST', processData: false, contentType: false, data: formData }).done(function(res) {
                        if (res.status) {
                            account.list.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Account was successfully added.</h6>')
                        } else {
                            accountValidator.showErrors({'name': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-account"]').attr('disabled', false)
                    })
                    return false
                }
            })
        }
    },
    view: {
        init(account_id) {
            $.get(`${VAR.baseUrl}account/${account_id}`).done(function(res) {
                if(res.status) {
                    res = res.data
                    res.baseUrl = VAR.baseUrl
                    res.token = document.cookie
                    eaControl.render(`app/account/account.view`, res, '#eaContent', function () {
                        $('[name="roles"]').selectpicker()
                        account.view.event(res, account_id)
                    })
                } else {
                    eaControl.notif('success', `<h5 class="title">Error !</h5> <h6 class="message">${res.message}</h6>`)
                }
            })
        },
        event(res, account_id) {
            const pictureValidator = $('#form-picture').validate({
                rules       : { "picture": "required" },
                messages    : { "picture": "Name is required" },
                invalidHandler: function (event, pictureValidator) {
                    const errors = pictureValidator.numberOfInvalids()
                    if (errors) {
                        $('#form-picture-card-error .alert').fadeIn("fast")
                    }
                },
                errorContainer: '#form-picture-card-error .alert',
                errorLabelContainer: '#formPictureErrorMessage',
                wrapper: 'span',
                submitHandler: function (form) {
                    /** Form picture */
                    const formData = new FormData($(form)[0]);
                    $('[aria-action="submit-picture"]').attr('disabled', true)
                    $.ajax({ url: `${VAR.baseUrl}account/${res.account.username}/save-picture`, method: 'PUT', processData: false, contentType: false, data: formData }).done(function(res) {
                        if (res.status) {
                            account.view.init(account_id)
                            profilePicture.attr('src', profilePicture.attr('src') + '&timestamp=' + new Date().getTime())
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Picture was successfully added.</h6>')
                        } else {
                            pictureValidator.showErrors({'picture': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-picture"]').attr('disabled', false)
                    })
                    return false
                }
            })

            const personalDataValidator = $('#form-personal-data').validate({
                rules       : { 
                    "name": "required",
                    "address": "required"
                },
                messages    : { 
                    "name": "Name is required",
                    "address": "Address is required"
                },
                invalidHandler: function (event, personalDataValidator) {
                    const errors = personalDataValidator.numberOfInvalids()
                    if (errors) {
                        $('#form-personal-data-card-error .alert').fadeIn("fast")
                    }
                },
                errorContainer: '#form-personal-data-card-error .alert',
                errorLabelContainer: '#formPersonalDataErrorMessage',
                wrapper: 'span',
                submitHandler: function (form) {
                    /** Form personal-data */
                    const formData = $(form).serialize()
                    $('[aria-action="submit-personal-data"]').attr('disabled', true)
                    $.ajax({ url: `${VAR.baseUrl}account/${account_id}/save-personal-data/`, method: 'PUT', data: formData }).done(function(res) {
                        if (res.status) {
                            account.view.init(account_id)
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Personal data was successfully edited.</h6>')
                        } else {
                            personalDataValidator.showErrors({'name': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-personal-data"]').attr('disabled', false)
                    })
                    return false
                }
            })

            const rolesValidator = $('#form-roles').validate({
                rules       : { 
                    "roles": "required",
                },
                messages    : { 
                    "roles": "Roles is required",
                },
                invalidHandler: function (event, rolesValidator) {
                    const errors = rolesValidator.numberOfInvalids()
                    if (errors) {
                        $('#form-roles-card-error .alert').fadeIn("fast")
                    }
                },
                errorContainer: '#form-roles-card-error .alert',
                errorLabelContainer: '#formRolesErrorMessage',
                wrapper: 'span',
                submitHandler: function (form) {
                    /** Form roles */
                    const formData = $(form).serialize()
                    $('[aria-action="submit-roles"]').attr('disabled', true)
                    $.ajax({ url: `${VAR.baseUrl}account/${account_id}/save-roles/`, method: 'PUT', data: formData }).done(function(res) {
                        if (res.status) {
                            account.view.init(account_id)
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Roles was successfully assigned.</h6>')
                        } else {
                            rolesValidator.showErrors({'roles': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-roles"]').attr('disabled', false)
                    })
                    return false
                }
            })

            const resetPasswordValidator = $('#form-reset-password').validate({
                rules       : { 
                    "password": "required",
                    "repassword": "required",
                },
                messages    : { 
                    "password": "Password is required",
                    "repassword": "Repassword is required",
                },
                invalidHandler: function (event, resetPasswordValidator) {
                    const errors = resetPasswordValidator.numberOfInvalids()
                    if (errors) {
                        $('#form-reset-password-card-error .alert').fadeIn("fast")
                    }
                },
                errorContainer: '#form-reset-password-card-error .alert',
                errorLabelContainer: '#formResetPasswordErrorMessage',
                wrapper: 'span',
                submitHandler: function (form) {
                    /** Form reset-password */
                    const formData = $(form).serialize()
                    $('[aria-action="submit-reset-password"]').attr('disabled', true)
                    $.ajax({ url: `${VAR.baseUrl}account/${account_id}/reset-password/`, method: 'PUT', data: formData }).done(function(res) {
                        if (res.status) {
                            account.view.init(account_id)
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Password was successfully reseted.</h6>')
                        } else {
                            resetPasswordValidator.showErrors({'password': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-reset-password"]').attr('disabled', false)
                    })
                    return false
                }
            })

            const resetUsernameValidator = $('#form-reset-username').validate({
                rules       : { "username": "required" },
                messages    : { "username": "Userrname is required" },
                invalidHandler: function (event, resetUsernameValidator) {
                    const errors = resetUsernameValidator.numberOfInvalids()
                    if (errors) {
                        $('#form-reset-username-card-error .alert').fadeIn("fast")
                    }
                },
                errorContainer: '#form-reset-username-card-error .alert',
                errorLabelContainer: '#formResetUserrnameErrorMessage',
                wrapper: 'span',
                submitHandler: function (form) {
                    /** Form reset-username */
                    const formData = $(form).serialize()
                    $('[aria-action="submit-reset-username"]').attr('disabled', true)
                    $.ajax({ url: `${VAR.baseUrl}account/${account_id}/reset-username/`, method: 'PUT', data: formData }).done(function(res) {
                        if (res.status) {
                            window.location.href = "#/account/"+res.username
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Userrname was successfully reseted.</h6>')
                        } else {
                            resetUsernameValidator.showErrors({'username': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-reset-username"]').attr('disabled', false)
                    })
                    return false
                }
            })
        }
    }
}