let settings = {
    view: {
        init() {
            $.get(`${VAR.baseUrl}settings`).done(function(res) {
                if(res.status) {
                    res = res.data
                    res.baseUrl = VAR.baseUrl
                    res.token = document.cookie
                    eaControl.render(`app/settings/settings.view`, res, '#eaContent', function () {
                        settings.view.event(res)
                    })
                } else {
                    eaControl.notif('success', `<h5 class="title">Error !</h5> <h6 class="message">${res.message}</h6>`)
                }
            })
        },
        event(res) {
            const pictureValidator = $('#form-picture').validate({
                rules       : { "picture": "required" },
                messages    : { "picture": "Name is required" },
                invalidHandler: function (event, pictureValidator) {
                    const errors = pictureValidator.numberOfInvalids()
                    if (errors) {
                        $('#form-picture-card-error .alert').fadeIn("fast")
                    }
                },
                errorContainer: '#form-picture-card-error .alert',
                errorLabelContainer: '#formPictureErrorMessage',
                wrapper: 'span',
                submitHandler: function (form) {
                    /** Form picture */
                    const formData = new FormData($(form)[0]);
                    $('[aria-action="submit-picture"]').attr('disabled', true)
                    $.ajax({ url: `${VAR.baseUrl}settings/save-picture`, method: 'PUT', processData: false, contentType: false, data: formData }).done(function(res) {
                        if (res.status) {
                            settings.view.init()
                            let profilePicture = $('[data-image="profile"]')
                            profilePicture.attr('src', profilePicture.attr('src') + '&timestamp=' + new Date().getTime())
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Picture was successfully added.</h6>')
                        } else {
                            pictureValidator.showErrors({'picture': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-picture"]').attr('disabled', false)
                    })
                    return false
                }
            })

            const personalDataValidator = $('#form-personal-data').validate({
                rules       : { 
                    "name": "required",
                    "address": "required"
                },
                messages    : { 
                    "name": "Name is required",
                    "address": "Address is required"
                },
                invalidHandler: function (event, personalDataValidator) {
                    const errors = personalDataValidator.numberOfInvalids()
                    if (errors) {
                        $('#form-personal-data-card-error .alert').fadeIn("fast")
                    }
                },
                errorContainer: '#form-personal-data-card-error .alert',
                errorLabelContainer: '#formPersonalDataErrorMessage',
                wrapper: 'span',
                submitHandler: function (form) {
                    /** Form personal-data */
                    const formData = $(form).serialize()
                    $('[aria-action="submit-personal-data"]').attr('disabled', true)
                    $.ajax({ url: `${VAR.baseUrl}settings/save-personal-data/`, method: 'PUT', data: formData }).done(function(res) {
                        if (res.status) {
                            settings.view.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Personal data was successfully saved.</h6>')
                        } else {
                            personalDataValidator.showErrors({'name': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-personal-data"]').attr('disabled', false)
                    })
                    return false
                }
            })
            
            const resetPasswordValidator = $('#form-reset-password').validate({
                rules       : { 
                    "password": "required",
                    "repassword": "required",
                },
                messages    : { 
                    "password": "Password is required",
                    "repassword": "Repassword is required",
                },
                invalidHandler: function (event, resetPasswordValidator) {
                    const errors = resetPasswordValidator.numberOfInvalids()
                    if (errors) {
                        $('#form-reset-password-card-error .alert').fadeIn("fast")
                    }
                },
                errorContainer: '#form-reset-password-card-error .alert',
                errorLabelContainer: '#formResetPasswordErrorMessage',
                wrapper: 'span',
                submitHandler: function (form) {
                    /** Form reset-password */
                    const formData = $(form).serialize()
                    $('[aria-action="submit-reset-password"]').attr('disabled', true)
                    $.ajax({ url: `${VAR.baseUrl}settings/reset-password/`, method: 'PUT', data: formData }).done(function(res) {
                        if (res.status) {
                            settings.view.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Password was successfully reseted.</h6>')
                        } else {
                            resetPasswordValidator.showErrors({'password': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-reset-password"]').attr('disabled', false)
                    })
                    return false
                }
            })

            const resetUsernameValidator = $('#form-reset-username').validate({
                rules       : { "username": "required" },
                messages    : { "username": "Userrname is required" },
                invalidHandler: function (event, resetUsernameValidator) {
                    const errors = resetUsernameValidator.numberOfInvalids()
                    if (errors) {
                        $('#form-reset-username-card-error .alert').fadeIn("fast")
                    }
                },
                errorContainer: '#form-reset-username-card-error .alert',
                errorLabelContainer: '#formResetUserrnameErrorMessage',
                wrapper: 'span',
                submitHandler: function (form) {
                    /** Form reset-username */
                    const formData = $(form).serialize()
                    $('[aria-action="submit-reset-username"]').attr('disabled', true)
                    $.ajax({ url: `${VAR.baseUrl}settings/reset-username/`, method: 'PUT', data: formData }).done(function(res) {
                        if (res.status) {
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Userrname was successfully reseted.</h6>')
                            window.location.reload()
                        } else {
                            resetUsernameValidator.showErrors({'username': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-reset-username"]').attr('disabled', false)
                    })
                    return false
                }
            })
        }
    }
}