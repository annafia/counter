let customer = {
    init() {
        $.get(`${VAR.baseUrl}customer`).done(function(res) {
            if(res.status) {
                res.customer = res.data
                res.baseUrl = VAR.baseUrl
                eaControl.render(`app/customer/customer.list`, res, '#eaContent', function () {
                    $('#table-list-customer').DataTable()
                    customer.event(res.customer)
                })
            } else {
                eaControl.notif('success', `<h5 class="title">Error !</h5> <h6 class="message">${res.message}</h6>`)
            }
        })
    },
    event(res) {
        let editElement = null

        $('#table-list-customer').on('click', '[aria-action="delete-customer"]', function() {
            let customer_id = $(this).data('id')
            eaControl.notif('confirm', '<h5 class="title">Warning !</h5>'+
                '<h6 class="message">Are you sure you want to delete this customer ?</h6>', function() {
                /** --------------- Do action here --------------- */
                $.ajax({ url: `${VAR.baseUrl}customer/${customer_id}`, method: 'DELETE' }).done(function(res) {
                    if (res.status) {
                        customer.init()
                        eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Customer was successfully deleted.</h6>')
                    } else {
                        eaControl.notif('error', '<h5 class="title">Error !</h5><h6 class="message">'+res.message+'</h6>')
                    }
                })
            })
        })

        $('#table-list-customer').on('click', '[aria-action="edit-customer"]', function() {
            $('[container="form-subject"]').html('Edit')
            $('[aria-action="cancel-edit-customer"]').fadeIn()

            let customer_id = $(this).data('id')
            for (let i = 0; i < res.length; i++) {
                let row = res[i]
                if(row.id == customer_id) { editElement = row }
            }

            $('[name="name"]').val(editElement.name)
            $('[name="email"]').val(editElement.email)
            $('[name="address"]').val(editElement.address)
            $('[name="phone"]').val(editElement.phone)
            $('[name="name"]').focus()
        })

        $('[aria-action="cancel-edit-customer"]').click(function() {
            $('[container="form-subject"]').html('Add')
            $('[aria-action="cancel-edit-customer"]').fadeOut()
            $('[name="name"]').val('')
            $('[name="email"]').val('')
            $('[name="address"]').val('')
            $('[name="phone"]').val('')
            $('[name="name"]').focus()
            editElement = null
        })

        const customerValidator = $('#form-customer').validate({
            rules       : { 
                "name": "required",
                "email": "required",
                "phone": "required",
                "address": "required",
            },
            messages    : { 
                "name": "Name is required",
                "address": "Address is required",
                "email": "Email is required",
                "phone": "Phone number is required",
            },
            invalidHandler: function (event, customerValidator) {
                const errors = customerValidator.numberOfInvalids()
                if (errors) {
                    $('#form-customer-card-error .alert').fadeIn("fast")
                }
            },
            errorContainer: '#form-customer-card-error .alert',
            errorLabelContainer: '#formCustomerErrorMessage',
            wrapper: 'span',
            submitHandler: function (form) {
                /** Form customer */
                const formData = $(form).serialize()
                $('[aria-action="submit-customer"]').attr('disabled', true)
                if(!editElement) {
                    $.ajax({ url: `${VAR.baseUrl}customer`, method: 'POST', data: formData }).done(function(res) {
                        if (res.status) {
                            customer.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Customer was successfully added.</h6>')
                        } else {
                            customerValidator.showErrors({'name': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-customer"]').attr('disabled', false)
                    })
                } else {
                    $.ajax({ url: `${VAR.baseUrl}customer/${editElement.id}`, method: 'PUT', data: formData }).done(function(res) {
                        if (res.status) {
                            customer.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Customer was successfully edited.</h6>')
                        } else {
                            customerValidator.showErrors({'name': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-customer"]').attr('disabled', false)
                    })
                }
                return false
            }
        })
    }
}