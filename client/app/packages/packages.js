let packages = {
    init() {
        $.get(`${VAR.baseUrl}package`).done(function(res) {
            if(res.status) {
                res = res.data
                eaControl.render(`app/packages/packages.list`, res, '#eaContent', function () {
                    $('#table-list-packages').DataTable()
                    $('[name="provider_id"]').selectpicker()
                    packages.event(res)
                })
            } else {
                eaControl.notif('success', `<h5 class="title">Error !</h5> <h6 class="message">${res.message}</h6>`)
            }
        })
    },
    event(res) {
        let editElement = null

        $('#table-list-packages').on('click', '[aria-action="delete-packages"]', function() {
            let packages_id = $(this).data('id')
            eaControl.notif('confirm', '<h5 class="title">Warning !</h5>'+
                '<h6 class="message">Are you sure you want to delete this packages ?</h6>', function() {
                /** --------------- Do action here --------------- */
                $.ajax({ url: `${VAR.baseUrl}package/${packages_id}`, method: 'DELETE' }).done(function(res) {
                    if (res.status) {
                        packages.init()
                        eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Package was successfully deleted.</h6>')
                    } else {
                        eaControl.notif('error', '<h5 class="title">Error !</h5><h6 class="message">'+res.message+'</h6>')
                    }
                })
            })
        })

        $('#table-list-packages').on('click', '[aria-action="edit-packages"]', function() {
            $('[container="form-subject"]').html('Edit')
            $('[aria-action="cancel-edit-packages"]').fadeIn()

            let packages_id = $(this).data('id')
            for (let i = 0; i < res.packages.length; i++) {
                let row = res.packages[i]
                if(row.id == packages_id) { editElement = row }
            }
            $('[name="provider_id"]').val(editElement.provider_id)
            $('[name="provider_id"]').selectpicker('refresh')

            $('[name="package_name"]').val(editElement.package_name)
            $('[name="purchase"]').val(editElement.purchase)
            $('[name="sell"]').val(editElement.sell)
            $('[name="package_name"]').focus()
        })

        $('[aria-action="cancel-edit-packages"]').click(function() {
            $('[container="form-subject"]').html('Add')
            $('[aria-action="cancel-edit-packages"]').fadeOut()
            
            $('[name="provider_id"]').val('')
            $('[name="provider_id"]').selectpicker('refresh')

            $('[name="package_name"]').val('')
            $('[name="purchase"]').val('')
            $('[name="sell"]').val('')
            $('[name="package_name"]').focus()
            editElement = null
        })

        const packagesValidator = $('#form-packages').validate({
            rules       : { 
                "package_name": "required",
                "provider_id": "required",
                "purchase": "required",
                "sell": "required"
            },
            messages    : { 
                "package_name": "Package name is required",
                "provider_id": "Provider is required",
                "purchase": "Purchase price is required",
                "sell": "Sell price is required"
            },
            invalidHandler: function (event, packagesValidator) {
                const errors = packagesValidator.numberOfInvalids()
                if (errors) {
                    $('#form-packages-card-error .alert').fadeIn("fast")
                }
            },
            errorContainer: '#form-packages-card-error .alert',
            errorLabelContainer: '#formPackagesErrorMessage',
            wrapper: 'span',
            submitHandler: function (form) {
                /** Form packages */
                const formData = $(form).serialize()
                $('[aria-action="submit-packages"]').attr('disabled', true)
                if(!editElement) {
                    $.ajax({ url: `${VAR.baseUrl}package`, method: 'POST', data: formData }).done(function(res) {
                        if (res.status) {
                            packages.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Package was successfully added.</h6>')
                        } else {
                            packagesValidator.showErrors({'package_name': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-packages"]').attr('disabled', false)
                    })
                } else {
                    $.ajax({ url: `${VAR.baseUrl}package/${editElement.id}`, method: 'PUT', data: formData }).done(function(res) {
                        if (res.status) {
                            packages.init()
                            eaControl.notif('success', '<h5 class="title">Success !</h5><h6 class="message">Package was successfully edited.</h6>')
                        } else {
                            packagesValidator.showErrors({'package_name': JSON.stringify(res.message)})
                        }
                    }).always(function() {
                        $('[aria-action="submit-packages"]').attr('disabled', false)
                    })
                }
                return false
            }
        })
    }
}