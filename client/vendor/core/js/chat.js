/**
* @copyright : 2018, PT. Elnusa, Tbk.
* @url       : www.elnusa.co.id
* @author    : Harry Surya Kurniawan
* @version   : 2.0
*/


const click = new Audio('/appframework/assets/sound/chat/whatsapp_web.mp3');

eaController.prototype.initChat = function(callback) {
    const env  = window.location.host,
          host = (env === 'localhost' || this.isIP(env) ? (window.location.pathname).replace('/index.html', '') : env.replace('.elnusa.co.id', ''));

    const socketConf = {
        query: {
            host     : (this.chatConfig.host ? this.chatConfig.host : host),
            username : this.userinfo.username || this.userinfo.authusername,
            fullname : this.userinfo.name,
            photo    : this.userinfo.photo
        },
        url : (env == 'localhost' || env.includes('dev') ? 'http://devchat.elnusa.co.id' : 'https://chat.elnusa.co.id'),
        path: `/chat-api/socket.io`
    };

    this.createSocket('ea-chat', socketConf, () => {

        if(typeof callback === 'function') {
            callback();
        }

        this.chatSetEnv();

        $('#eaNavChatter').on('click', function(e) {
            e.preventDefault();
            $('#eaChatter').toggleClass('toggled');
            $(this).toggleClass('toggled');
        });

        this.sockets['ea-chat'].on('/send-message', (data) => {
            chatHelper.message.init(false, data);
        });

        this.sockets['ea-chat'].on('/history', (historydata) => {
            chatHelper.message.history(historydata);
        });

        this.sockets['ea-chat'].on('/check', (data) => {
            chatHelper.message.notification(data); 
        });

        this.sockets['ea-chat'].on('/config', (userid) => {
            this.chatConfig.userkey = userid;
        });
    });
}

// click Control
eaController.prototype.chatClickControl = function() {
    let control = this;

    // tab panel on click
    $(document).off('click', '.tab-panel-component').on('click', '.tab-panel-component', function() {
        let tab    = $(this),
            target = tab.attr('data-tab');

        $('.tab-panel-component.active').removeClass('active');
        $(`.chat-bottom-tabs`).removeClass('active');

        tab.addClass('active');
        $(`.chat-bottom-tabs#${target}`).addClass('active');
    });

    // contact on click
    $(document).off('click', '.chat-list > li.contact-card:not(.empty)').on('click', '.chat-list > li.contact-card:not(.empty)', function() {
        let contact = $(this),
            clone   = $(this).clone();

        let userdata = {
            username  : contact.attr('data-username'),
            targetkey : contact.attr('data-targetkey')
        };

        if($(`div#chat-message-list > ul.chat-list > li.contact-card`).length) {
            $.each($(`div#chat-message-list > ul.chat-list > li.contact-card`), function() {
                if(userdata.username === $(this).attr('data-username')) {
                    $(this).remove();
                }

                $('div#chat-message-list > ul.chat-list').prepend(clone);
            });
        }
        else {
            $('div#chat-message-list > ul.chat-list').prepend(clone);
        }

        if(!$(`div.message-panel-container[id="${userdata.username}"]`).length) {
            control.messagePanel(userdata);
        }
        else {
            $(`div.message-panel-container[id="${userdata.username}"] > div.message-header`).trigger('click');

            setTimeout(() => {
                document.getElementById(`input-${userdata.username}`).focus();
            }, 500);
        }
    });

    // close panel
    $(document).off('click', '.message-header > span.close-message').on('click', '.message-header > span.close-message', function() {
        let btn   = $(this),
            panel = btn.parent('div').parent('div');

        let user = panel.attr('id');

        control.sockets['ea-chat'].emit('/check', {
            fromkey: control.chatConfig.userkey,
            touser : user
        });

        panel.remove();
    });

    // online/offline panel
    $(document).off('click', '.chat-list-container#main-container > li > div.chat-list-separator').on('click', '.chat-list-container#main-container > li > div.chat-list-separator', function() {
        let panel     = $(this),
            id        = panel.parent('li').attr('class'),
            container = panel.parent('li').find('> .status-container');

        if(container.hasClass('toggled')) {
            container.removeClass('toggled');

            $(`.chat-list-separator[data-collapse="${id}"] > i`).css({
                '-moz-transform': 'rotate(0deg)',
                '-webkit-transform': 'rotate(0deg)',
                'transform': 'rotate(0deg)'
            });
        }
        else {
            container.addClass('toggled');
            
            $(`.chat-list-separator[data-collapse="${id}"] > i`).css({
                '-moz-transform': 'rotate(90deg)',
                '-webkit-transform': 'rotate(90deg)',
                'transform': 'rotate(90deg)'
            });
        }

        container.toggle('fast');
    });

    // message panel header on click
    $(document).off('click', `div.message-header`).on('click', `div.message-header`, function() {
        let header    = $(this),
            container = header.parent('div.message-panel-container'),
            target    = container.find('> div.message-main-container > div.message-footer');

        if(container.hasClass('toggled')) {
            container.animate({
                'height'    : "300px"
            }, 300, () => {
                target.show();
                container.removeClass('toggled');

                control.sockets['ea-chat'].emit('/check', {
                    fromkey: control.chatConfig.userkey,
                    touser : container.attr('id')
                });
            });
        }
        else {
            target.hide();

            container.animate({
                'height'    : "30px",
            }, 300, () => {
                container.addClass('toggled');
            });
        }
    });

    // on search
    $(document).off('keyup', `input#chat-search-bar`).on('keyup', `input#chat-search-bar`, function() {
        if(typeof control.chatConfig.onsearch === 'function') {
            control.chatConfig.onsearch();
        }
        else {
            let value     = $(this).val(),
                container = $('li.chat-seacrh-container');

            if(value === '') {
                container.hide();

                container.find(`li.contact-card.empty`).remove();
                $(`li.online-user, li.offline-user`).show();
                $('li.chat-seacrh-container > div#search-list > ul.chat-list').html('');
            }
            else {
                container.show();

                $(`li.online-user, li.offline-user`).hide();
            }

            return new Promise((resolve) => {
                let body = ``,
                    temp = ``;

                $.each($(`li.online-user > div#online-user > ul.chat-list > li.contact-card:not(.empty), li.offline-user > div#offline-user > ul.chat-list > li.contact-card:not(.empty)`), function(i) {

                    if($(this).attr('data-username').includes(value)) {
                        temp = $(this)[0].outerHTML;

                        if($(this).find(`.chat-status-indicator`).hasClass('online')) {
                            temp = temp.replace(`class="contact-card"`, `class="contact-card online search-card"`);
                        }

                        body += temp;
                    }

                    if(i == $(`li.online-user > div#online-user > ul.chat-list > li.contact-card:not(.empty), li.offline-user > div#offline-user > ul.chat-list > li.contact-card:not(.empty)`).length - 1) {
                        resolve(body);
                    }
                });
            }).then((body) => {
                if(body === '') {
                    body = `<li class="contact-card empty" style="text-align: center;">No Contact Found</li>`;
                }

                $('li.chat-seacrh-container > div#search-list > ul.chat-list').html(body);
            });
        }
    });
}

// set Environment
eaController.prototype.chatSetEnv = function(icon, dustdata) {
    this.icon = {
        property : {}
    }

    switch(icon) {
        case 'icofont':
            this.icon.property.bubble  = 'icofont-ui-messaging';
            this.icon.property.carret  = 'icofont-circled-right';
            this.icon.property.close   = 'icofont-close';
            this.icon.property.history = 'icofont-history';
            this.icon.property.user    = 'icofont-user-alt-3';
            this.icon.property.search  = 'icofont-search';
            this.icon.property.send    = 'icofont-paper-plane';
        break;
        case 'themify':
            this.icon.property.bubble  = 'ti-comments';
            this.icon.property.carret  = 'ti-arrow-circle-right';
            this.icon.property.close   = 'ti-close';
            this.icon.property.history = 'ti-time';
            this.icon.property.user    = 'ti-user';
            this.icon.property.search  = 'ti-search';
            this.icon.property.send    = 'ti-angle-right';
        break;
        default:
            this.icon.property.bubble  = 'mdi mdi-forum';
            this.icon.property.carret  = 'mdi mdi-arrow-right-drop-circle-outline';
            this.icon.property.close   = 'mdi mdi-close';
            this.icon.property.history = 'mdi mdi-history';
            this.icon.property.user    = 'mdi mdi-account';
            this.icon.property.search  = 'mdi mdi-magnify';
            this.icon.property.send    = 'mdi mdi-send';
        break;
    }

    let data = {
        icon    : this.icon.property
    };

    data = Object.assign(data, dustdata);

    this.chatClickControl();
    this.onlineUser(data);
}

// Online user
eaController.prototype.onlineUser = function() {
    
    let ulist   = [],
        control = this,
        init    = {
            online  : [],
            offline : [] 
        };

    this.sockets['ea-chat'].on('/onlineuser', (data) => {
        let temparray = [],
            tempObj   = [],
            contacts  = (this.chatConfig !== undefined ? this.chatConfig.contacts : {}),
            current, username, container;

        let empty = `<li class="contact-card empty">
                         No Contact Available
                     </li>`;

        if(!$('div.chat-side-content').length) {
            $.each(data, function(i, v) {
                temparray.push(v.fromuser);

                tempObj[v.fromuser] = {
                    socketid    : v.socketid,
                    userkey     : i,
                    displayname : v.displayname
                };
            });

            $.each(contacts, function(i,v) {
                if(contacts[i].username == control.userinfo.username || contacts[i].username == control.userinfo.authusername) {
                    return;
                }

                if(temparray.includes(v.username)) {
                    contacts[i].targetkey = tempObj[v.username].userkey;

                    init.online.push(v);
                }
                else {
                    init.offline.push(v);
                }
                
                ulist.push(v.username);
            });

            init.icon = this.icon.property;

            this.render(this.templates.messenger, init, '#eaChatter', () => {
                $.each(data, function(i, v) {
                    $(`li.contact-card[data-username="${v.fromuser}"]`).find('.chat-status-indicator').addClass('online');
                });

                $.each($(`.chat-status-indicator.online`), function(i, v) {
                    current  = $(this);
                    username = current.closest('li').attr('data-username');

                    if(!temparray.includes(username)) {
                        current.removeClass('online');
                    }
                });
            });
        }
        else {
            $.each(data, function(i, v) {
                $(`div#chat-contact-list, div#chat-message-list`).find(`li.contact-card[data-username="${v.fromuser}"]`).find('.chat-status-indicator').addClass('online');
                
                let found = false, _this;

                if(!$('.status-container#online-user > ul > li.contact-card:not(.empty)').length) {
                    $(`div#chat-contact-list`).find(`div#offline-user li.contact-card[data-username="${v.fromuser}"]`).appendTo($('.status-container#online-user > ul'));

                    $('.status-container#online-user > ul > li.contact-card.empty').remove();
                }
                else {
                    $.each($('div.status-container#online-user > ul > li'), function() {
                        _this = $(this);

                        if(!found) {
                            if(_this.attr('data-username') > v.fromuser) {
                                $(`div#chat-contact-list`).find(`div#offline-user li.contact-card[data-username="${v.fromuser}"]`).insertBefore(_this);

                                found = true;
                                return;
                            }
                        }
                        else {
                            return;
                        }

                        if(i == ($('.status-container#online-user > ul > li').length - 1) && !found) {
                            $(`div#chat-contact-list`).find(`div#offline-user li.contact-card[data-username="${v.fromuser}"]`).insertAfter(_this);
                        }
                    });
                }

                if(!$('.status-container#offline-user > ul > li').length) {
                    $('.status-container#offline-user > ul').append(empty);
                }
                else {
                    $('.status-container#offline-user > ul > li.contact-card.empty').remove();
                }

                temparray.push(v.fromuser);
            });

            $.each($(`div#chat-contact-list`).find(`.chat-status-indicator.online`), function(i, v) {
                current   = $(this);
                container = current.closest('li:not(.search-card)');
                username  = container.attr('data-username');
                index     = ulist.indexOf(username);

                if(!temparray.includes(username)) {
                    current.removeClass('online');
                    $(`div#chat-message-list`).find(`li[data-username="${username}"] .chat-status-indicator.online`).removeClass('online');

                    let found = false, _this;

                    if(!$('.status-container#online-user > ul > li.contact-card:not(.empty)').length) {
                        container.appendTo($('.status-container#online-user > ul'));

                        $('.status-container#offline-user > ul > li.contact-card.empty').remove();
                    }
                    else {
                        $.each($('.status-container#offline-user > ul > li'), function(i,v) {
                            _this = $(this);

                            if(!found) {
                                if(_this.attr('data-username') > container.attr('data-username')) {

                                    container.insertBefore(_this);

                                    found = true;
                                    return;
                                }
                            }
                            else {
                                return;
                            }

                            if(!($('.status-container#offline-user > ul > li').length) || i == ($('.status-container#offline-user > ul > li').length - 1) && !found) {
                                container.insertAfter(_this);
                            }
                        });
                    }

                    if(!$('.status-container#online-user > ul > li').length) {
                        $('.status-container#online-user > ul').append(empty);
                    }
                    else {
                        $('.status-container#online-user > ul > li.contact-card.empty').remove();
                    }
                }
            });
        }
    });
}

// Create Message Panel
eaController.prototype.messagePanel = function(userdata) {
    let container = $('div.chat-message-panel'),
        user      = userdata.username,
        userkey   = userdata.targetkey,
        template  = `<div class="message-panel-container" id="${user}" data-targetkey="${userkey}">
                         <div class="message-header">
                             ${user}
                             <span class="close-message"><i class="${this.icon.property.close}"></i></span>
                         </div>
                         <div class="message-main-container">
                            <div class="message-body">
                                
                            </div>
                            <div class="message-footer">
                                <textarea class="message-area" id="input-${user}" placeholder="Type messsage..."></textarea>
                                <button type="button" class="send-message" id="submit-${user}"><i class="${this.icon.property.send}"></i></button>
                            </div>
                         </div>
                     </div>`;

    if(container.find('> div.message-panel-container').length == 3) {
        container.find('> div.message-panel-container:nth-last-child(1)').remove();
    }

    container.prepend(template);

    chatHelper.textarea.init(`input-${user}`);

    $(document).off('click', `button[id="submit-${user}"]`).on('click', `button[id="submit-${user}"]`, function() {
        let input   = $(`textarea[id="input-${user}"]`);
        let payload = {
            target    : user,
            targetkey : userkey,
            message   : JSON.stringify(input.val())
        };

        if(input.val() !== "") {
            chatHelper.message.init(true, payload);
        }

        input.val('');
    });

    this.sockets['ea-chat'].emit('/history', {
        tokey  : userkey,
        touser : user
    });
}


// Chat Helper
const chatHelper = {
    textarea : {
        observe: {},

        // initiate custom event for textarea
        init: (elementid) => {

            if (window.attachEvent) {
                chatHelper.textarea.observe = (element, event, handler) => {
                    element.attachEvent('on' + event, handler);
                };
            }
            else {
                chatHelper.textarea.observe = (element, event, handler) => {
                    element.addEventListener(event, handler, false);
                };
            }

            chatHelper.textarea.track(elementid);
        },

        // track textarea content size and button handler for textarea. 
        // (enter) for submit message, (ctrl+enter) for new line.
        track: (elementid) => {
            let text = document.getElementById(elementid);

            let resize = () => {
                text.style.height = '35px';
                text.style.height = text.scrollHeight + 'px';
            }

            let delayedResize = () => {
                window.setTimeout(resize, 0);
            }

            chatHelper.textarea.observe(text, 'change',  resize);
            chatHelper.textarea.observe(text, 'cut',     delayedResize);
            chatHelper.textarea.observe(text, 'paste',   delayedResize);
            chatHelper.textarea.observe(text, 'drop',    delayedResize);
            chatHelper.textarea.observe(text, 'keydown', delayedResize);

            text.focus();
            text.select();

            $(`textarea[id="${elementid}"]`).keypress(function(e) {
                if(e.keyCode == 13 && e.shiftKey) {
                    let content = $(this).val(),
                        caret   = chatHelper.textarea.trackCarret($(this));

                    $(this).val(content.substring(0, caret) + content.substring(caret, content.length));

                    e.stopPropagation();
                }
                else if(e.keyCode == 13) {
                    e.preventDefault();

                    let id = elementid.replace('input-', 'submit-');

                    $(`button[id="${id}"]`).trigger('click');
                    resize();
                }
            });

            resize();
        },

        // track the carret position on text area
        trackCarret: (el) => {
            if (el.selectionStart) {
                return el.selectionStart;
            }
            else if (document.selection) {
                el.focus();

                let r = document.selection.createRange();

                if (r == null) {
                    return 0;
                } 

                let re = el.createTextRange(),
                    rc = re.duplicate();

                re.moveToBookmark(r.getBookmark());
                rc.setEndPoint('EndToStart', re);

                return rc.text.length;
            }

            return 0;
        }
    },

    message: {
        // message's bubble template
        bubble: (isOwnChat, content) => {
            return `<div class="chat-bubble-container${isOwnChat ? " bubble-own" : ""}">
                        <div class="chat-bubble${isOwnChat ? "" : " bubble-green"}">
                            ${chatHelper.htmlescape(content.body)}
                            <div class="chat-bubble-time">
                                ${content.time}
                            </div>
                        </div>
                    </div>`;
        },

        // create message bubble on send/receive message
        init: (isOwnChat, payload) => {
            let message = JSON.parse(payload.message),
                content = {},
                control = this.eaControl;
                target  = (isOwnChat ? payload.target : payload.fromuser);

            message = message.replace(/\n/g, '</br>');

            let d = new Date();

            content = {
                body : message,
                date : `Today`,
                time : `${d.getHours()}:${d.getMinutes()}`
            }

            let template = chatHelper.message.bubble(isOwnChat, content);

            return new Promise((resolve, reject) => {
                if(isOwnChat) {
                    let socketPayload = {
                        message  : payload.message,
                        tokey    : payload.targetkey,
                        touser   : payload.target,
                        fromuser : control.userinfo.username || control.userinfo.authusername,
                        fromkey  : control.chatConfig.userkey,
                    }

                    control.sockets['ea-chat'].emit('/send-message', socketPayload);
                }

                $(`div[id="${target}"] > div.message-main-container > div.message-body`).append(template);

                resolve(payload.target);
            }).then((targetkey) => {
                if(!$(`div[id="${target}"] > div.message-main-container > div.message-body`).length) {
                    control.sockets['ea-chat'].emit('/check', {
                        fromkey: control.userinfo.username || control.userinfo.authusername,
                        touser : targetkey
                    });
                }

                $(`div[id="${target}"] > div.message-main-container > div.message-body`).animate({
                    scrollTop: $(`div[id="${target}"] > div.message-main-container > div.message-body`).prop("scrollHeight") - $(`div[id="${target}"] > div.message-main-container > div.message-body`).height()
                }, "fast");
            });
        },

        // get and render chat history on message panel
        history: (records) => {
            let message, isOwnChat, target, date, time, content, curDate,
                chatBody = ``,
                control  = this.eaControl;;

            return new Promise((resolve, reject) => {
                $.each(records, function(i,v) {
                    isOwnChat = false;

                    message = JSON.parse(v.message);
                    message = message.replace(/\n/g, '</br>');

                    date = (v.timespan).split(' ');

                    time = date[1].split(':');
                    date = date[0];

                    if(v.fromuser === control.userinfo.username || v.fromuser === control.userinfo.authusername) {
                        isOwnChat = true;
                        target    = v.touser;
                    }
                    else {
                        target = v.fromuser;
                    }

                    content = {
                        body : message,
                        date : date,
                        time : `${time[0]}:${time[1]}`
                    };

                    if(i === 0 || content.date != curDate) {
                        curDate = content.date;

                        chatBody += `<div class="chat-time-container">
                                         <div class="chat-time-separator">${curDate}</div>
                                     </div>`;
                    }

                    chatBody += chatHelper.message.bubble(isOwnChat, content);

                    if(i == (records.length - 1)) {
                        resolve({
                            body   : chatBody,
                            target : target
                        });
                    }
                });
            }).then((data) => {
                return new Promise((reject, resolve) => {
                    $(`div[id="${data.target}"] > div.message-main-container > div.message-body`).append(data.body);
                    $('div#chat-message-list > ul.chat-list').find(`li[data-username="${data.target}"] .en-chat-body-notif`).html('');
                    $('div#chat-message-list > ul.chat-list').find(`li[data-username="${data.target}"] .en-chat-body-notif`).addClass('empty');

                    $(`div[id="${data.target}"] > div.message-main-container > div.message-body`).animate({
                        scrollTop: $(`div[id="${data.target}"] > div.message-main-container > div.message-body`).prop("scrollHeight") - $(`div[id="${data.target}"] > div.message-main-container > div.message-body`).height()
                    }, 0);
                });
            });
        },

        // notification handler (for unread message)
        notification: (data) => {
            let notifHeader = $(`div.en-chat-header-notif`),
                body        = ``,
                html        = ``,
                counter     = 0,
                control     = this.eaControl;;

            click.currentTime = 0;
            click.pause();

            setTimeout(() => {
                let container = $('div#chat-message-list > ul.chat-list');

                $.each(data.unread, function(i,v) {
                    counter++;

                    if(!container.find(`li[data-username="${i}"]`).length && $(`li[data-username="${i}"]`)[0] !== undefined) {
                        html = $(`li[data-username="${i}"]`)[0].outerHTML;
                        html = html.replace(` data-value="0">`, `>${v}`);

                        body += html;
                    }
                    else {
                        if(!$(`div.message-panel-container[id="${i}"]`).length || ($(`div.message-panel-container[id="${i}"]`).length) && $(`div.message-panel-container[id="${i}"]`).hasClass('toggled')) {
                            container.find(`li[data-username="${i}"] .en-chat-body-notif`).html(v);
                        }
                        else {
                            counter--;

                            control.sockets['ea-chat'].emit('/check', {
                                fromkey: control.chatConfig.userkey,
                                touser : i
                            });
                        }
                    }

                    setTimeout(() => {
                        container.find(`li.contact-card[data-username="${i}"] .en-chat-body-notif`).removeClass('empty');
                    }, 500);
                });

                if(counter > 0) {
                    notifHeader.removeClass('notif-empty');
                    notifHeader.html(counter);

                    click.play();
                }
                else {
                    notifHeader.addClass('notif-empty');

                    $.each($('div#chat-message-list .en-chat-body-notif'), function() {
                        $(this).html('');
                        $(this).addClass('empty');
                    });
                }

                if(body.length) {
                    container.append(body);
                }
            }, 500);
        }
    },

    htmlescape : (html) => {
        return String(html)
               .replace(/&(?!\w+;)/g, '&amp;')
               .replace(/</g, '&lt;')
               .replace(/>/g, '&gt;')
               .replace(/"/g, '&quot;')
               .replace(/&lt;\/br&gt;/g, '</br>');
    }
}