(function() {
    'use strict';

    if (!document.cookie) {
        return window.location.href = `login.html?err=401`;
    }

    window.core = function(callback) {
        this.eaUrl   = VAR.eaUrl;
        this.baseUrl = VAR.baseUrl;
        this.appName = VAR.appName;
        this.brand   = VAR.brand;

        this.userinfo = {};
        this.templates = {
            header    : VAR.headerTemplate  || `/appcore/template/header`,
            content   : VAR.contentTemplate || `/appcore/template/content`,
            messenger : '/appcore/template/chat',
            notif     : '/appcore/template/notif'
        };

        this.ajaxControl();
        this.initDust();
        this.initSammy();

        $('#eaHtmlBody').addClass('ea-loaded');
        
        const headerData = {
            appName           : this.appName,
            brand             : this.brand,
            userinfo          : this.userinfo,
            isUserInitEnabled : VAR.isUserInitEnabled || false
        };

        this.render(this.templates.header, headerData, '#eaHeader', () => {
            let _this = this;

            $('#ea-sidemenu-toggler').on('click', function(e) {
                e.preventDefault();
                $('#eaHeader, #eaBody').toggleClass('toggled');
            });

            $('#eaSignOut').on('click', function(e) {
                e.preventDefault();
                _this.logout();
            });
        });
        
        this.render(this.templates.content, headerData, '#eaBody', async () => {

            let _this = this;

            if (headerData.isUserInitEnabled) {
                try {
                    await $.ajax({
                        url      : `${this.baseUrl}/userinfo`,
                        type     : 'get',
                        dataType : 'json',
                        success  : (data) => {
                            // this.userinfo = Object.assign(this.userinfo, data[this.appName]);
                            this.userinfo = Object.assign(this.userinfo, data);
                            // this.userinfo.menu = VAR.menu
                            console.log(this.userinfo)

                            if (typeof callback === 'function') {
                                callback();
                            }
                        }
                    });
                }
                catch(err) {
                    new Error(err);
                }
            }
            else if (typeof callback === 'function') {
                callback();
            }
        });

        $('#eaPopNotif').on('click', function(e) {
            e.preventDefault();

            $(this).addClass('d-none');

            if (this.popNotifTimer) {
                clearTimeout(this.popNotifTimer);
            }
        });
    };

    // Log Out
    core.prototype.logout = function() {
        delete $.ajaxSettings.beforeSend;

        $.ajax({
            url      : `${this.baseUrl}/logout`,
            type     : 'POST',
            data     : {
                authorization: document.cookie
            },
            success  : () => {
                this.cookie.clear();
                return window.location.href = 'login.html';
            }
        });
    };

    // Notification
    core.prototype.notif = function(typ, message, callback) {
        const
            types   = ['info', 'danger', 'error', 'success', 'warning', 'primary', 'confirm', 'default', 'custom'],
            typearr = typ.split('-'),
            type    = typearr[0],
            size    = typearr.length > 1 && typearr[1] ? typearr[1] : 'lg';

        if (types.indexOf(type) === -1) {
            type = 'default';
        }

        if (type && message) {
            const title = type.toLowerCase().replace(/\b[a-z]/g, (letter) => {
                return letter.toUpperCase();
            });;

            const data = {
                type    : type,
                message : message,
                size    : size,
                title   : title
            };

            let template = this.templates.notif;

            if(type === `custom`) {
                template = message;
            }

            this.render(template, data, '#eaNotif', () => {
                if (type === 'confirm' && typeof callback === 'function') {
                    $(`button#eaNotifConfirm`).trigger(`focus`);

                    $('button#eaNotifConfirm').on('click', function(e) {
                        e.preventDefault();
                        $('#eaNotifModal').modal('hide');
                        callback();
                    });
                }
                else if(type === `custom` && typeof callback === 'function') {
                    callback();
                }
                else if (type === 'error') {
                    $('#eaNotifReload').on('click', function(e) {
                        e.preventDefault();
                        window.location.reload();
                    });
                }

                if ($('#eaNotifModal').hasClass('show')) {
                    $('#eaNotifModal').modal('hide');
                }

                $('#eaNotifModal').modal('show');

                $('#eaNotifModal').on('shown.bs.modal', function () { 
                    if ($('.modal-backdrop').length > 1) {
                        $('.modal-backdrop').not(':first').remove();
                    }
                });

                $('#eaNotifModal').on('hidden.bs.modal', function () {
                    if ($('.modal.show').length) {
                        if ($('.modal-backdrop').length > 1) {
                            for (let i = 0; i < $('.modal-backdrop').length - 1; i++) {
                                $('.modal-backdrop')[i].remove();
                            }
                        }
                    }
                    else {
                        $('.modal-backdrop').remove();
                    }
                });
            });
        }
    };

    // Notification
    core.prototype.modal = function(target, data, callback) {

        this.render(target, data, '#eaNotif', () => {
            if ($('#eaNotifModal').hasClass('show')) {
                $('#eaNotifModal').modal('hide');
            }

            $('#eaNotifModal').modal('show');

            $('#eaNotifModal').on('shown.bs.modal', function () { 
                if ($('.modal-backdrop').length > 1) {
                    $('.modal-backdrop').not(':first').remove();
                }
            });

            $('#eaNotifModal').on('hidden.bs.modal', function () {
                if ($('.modal.show').length) {
                    if ($('.modal-backdrop').length > 1) {
                        for (let i = 0; i < $('.modal-backdrop').length - 1; i++) {
                            $($('.modal-backdrop')[i]).remove();
                        }
                    }
                }
                else {
                    $('.modal-backdrop').remove();
                }
            });

            callback();
        });
    };

    // input error
    core.prototype.error_label = function(target, message) {

        let id;

        if(target.indexOf(`.`) > -1 || target.indexOf(`#`) > -1) {
            id = target.split(/\.|#/g)[1];
        }
        else {
            id = target;
        }

        const template = `<label class="input-error" id="${id}-error"><i class="mdi mdi-alert"></i> <label>${message}</label>.</label>`;

        if($(`#${id}-error`).length) {
            $(`#${id}-error > label`).html(message);
            return;
        }

        if($(target).parent(`div`).parent(`div`).hasClass(`input-wrapper`)) {
            $(target).parent(`div`).parent(`div`).after(template);

            return;
        }

        $(target).parent(`div`).after(template);
    }

    // Render Dust
    core.prototype.render = function(template, data, container, callback) {
        dust.render(template, data, function(error, output) {
            $(container).html(output);

            if (typeof callback === 'function') {
                callback.call(this);
            }
        });
    };

    // Pop Notification
    core.prototype.popNotif = function(type, message) {
        if (this.popNotifTimer) {
            clearTimeout(this.popNotifTimer);
        }

        $('#eaPopNotif').html(message).removeClass('d-none');

        this.popNotifTimer = setTimeout(() => {
            $('#eaPopNotif').addClass('d-none');
            clearTimeout(this.popNotifTimer)
        }, 1000);
    };

    // Init Sammy
    core.prototype.initSammy = function() {
        this.router = $.sammy(function() {
            this.element_selector = '#eaContent';
            this.debug = false;
        }); 
    };

    // Init Dust
    core.prototype.initDust = function() {
        if (dust) {
            dust.onLoad = function(template, callback) {
                $.ajax({
                    url: `${template}.html`,
                    type: 'get',
                    success: (data) => {
                        callback(undefined, data);
                    }
                });
            };

            dust.helpers.currency = (chunk, context, body, param) => {
                return chunk.write(() => {
                    const number = dust.helpers.tap(param.value, chunk, context);

                    if (!isNaN (parseFloat(number))) {
                        const parts = number.toString().split('.');
                        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');

                        return parts.join('.');
                    }

                    return number;
                });
            };

            dust.helpers.capitalize = (chunk, context, body, param) => {
                return chunk.write(() => {
                    const text = dust.helpers.tap(param.value, chunk, context);

                    if (typeof text === 'string') {
                        return text.toLowerCase().replace(/\b[a-z]/g, (letter) => {
                            return letter.toUpperCase();
                        });
                    }

                    return text;
                })
            };

            dust.helpers.loop = function(chunk, context, bodies, params) {
                let from = parseInt(dust.helpers.tap(params.from, chunk, context), 10) || 1,
                      to = parseInt(dust.helpers.tap(params.to, chunk, context), 10) || 1,
                      len = Math.abs(to - from) + 1,
                      increment = (to - from) / (len - 1) || 1;
  
                while(from !== to) {
                    chunk = bodies.block(chunk, context.push(from, from, len));
                    from += increment;
                }
  
                return chunk.render(bodies.block, context.push(from, from, len));
            };
        }
    };

    // Ajax Control
    core.prototype.ajaxControl = function() {
        let _this = this;

        $.ajaxSetup({
            cache      : false,
            beforeSend : (xhr) => {
                xhr.setRequestHeader('authapp', VAR.appName);
                xhr.setRequestHeader(`Authorization`, document.cookie);

                $('#eaHtmlBody').addClass('ea-loaded');
            }
        });

        $(document).ajaxStop(() => {
            $('#eaHtmlBody').removeClass('ea-loaded');
        });
        
        $(document).ajaxError((event, response, settings, error) => {
            const status = response.status;

            $('#eaHtmlBody').removeClass('ea-loaded');
            if (status === 401 || status === 403) {
                window.location = `login.html?err=${status}`;
            }
        });

        $(document).ajaxComplete(function (event, response, settings) {
            if (response.status >= 400) {
                let message = `<h4 class="fc-red">HTTP STATUS ${response.status} : ${response.statusText}</h4> 
                               <h5 class="fc-red"><small>URL : ${settings.url}</small><h5>
                               <hr></hr>
                               ${response.responseJSON ? response.responseJSON.message : response.responseText}
                               <br>`;

                _this.notif('error', message);
            }
        });
    };

    core.prototype.disableLoader = function() {
        $.ajaxSetup({
            cache      : false,
            beforeSend : (xhr) => {
                xhr.setRequestHeader('authapp', VAR.appName);
                xhr.setRequestHeader(`Authorization`, document.cookie);
            }
        });
    }

    core.prototype.initLoader = function() {
        $.ajaxSetup({
            cache      : false,
            beforeSend : (xhr) => {
                xhr.setRequestHeader('authapp', VAR.appName);
                xhr.setRequestHeader(`Authorization`, document.cookie);

                $('#eaHtmlBody').addClass('ea-loaded');
            }
        });
    }

    // Init Datatable
    core.prototype.initDatatables = function(obj, config, callback) {
        const option = {
            buttons    : false,
            select     : true,
            processing : false,
            serverSide : false,
            mode       : 'client',
            order      : [[1, 'asc']]
        };

        if(config) {
            option.buttons     = config.buttons     || option.buttons;
            option.columndDefs = config.columndDefs || null;
            option.columns     = config.columns     || null;
            option.select      = config.select      || option.select;
            option.order       = config.order       || option.order;
        }

        const dtConfig = {
            dom: ` <"gd gd-row2 mb-1" <"p-1" l>${option.buttons ? `<"col-4" f><"col-4" B>`:`<"p-1 text-right" f>`}> 
                        t 
                    <"gd gd-row2" <"p-1" i><"p-1 text-right" p>>`,
            responsive : true,
            pagingType : 'full_numbers',
            buttons    : option.buttons,
            autoWidth  : true,
            ordering   : true,
            scrollX    : false,
            select     : option.select,
            columnDefs : option.columndDefs,
            columns    : option.columns,
            order      : option.order,
            processing : option.processing,
            serverSide : option.serverSide,
            scrollCollapse : false,
            drawCallback   : function() {
                const dt = this;

                if(typeof callback === 'function') {
                    callback(this);
                }

                $(`${obj}_filter input`).off('keyup keypress input paste').on('keyup', function (e) {
                    e.preventDefault();

                    if (e.keyCode === 13) {
                        dt.fnFilter(this.value);
                    }
                });
            }
        };

        if(config.mode && config.mode === 'server') {
            dtConfig.processing = true;
            dtConfig.serverSide = true;
            dtConfig.url        = config.ajax.url;

            dtConfig.ajax = (ajaxData, ajaxCallback, ajaxSettings) => {
                if(ajaxData.search.value) {
                    this.disableLoader();
                }

                ajaxSettings.jqXHR = $.ajax({
                    url      : config.ajax.url,
                    type     : 'GET',
                    dataType : 'JSON',
                    data     : ajaxData,
                    cache    : false,
                    dataSrc  : (json) => {
                        // return json[this.appName].datatable;
                        return json.datatable;
                    },
                    success  : (data) => {
                        if(ajaxData.search.value) {
                            this.initLoader();
                        }

                        // ajaxCallback(data[this.appName]);
                        ajaxCallback(data);
                    }
                });
            };
        }

        return $(obj).DataTable(dtConfig);
    };

    core.prototype.cookie = {
        set : function(name, value, days) {
            const expires = ``;

            if(days) {
                const date = new Date();

                date.setTime(date.getTime() + (days*24*60*60*1000));

                expires = `; expires=${date.toUTCString()}`;
            }

            document.cookie = `${name} = ${(value || '')}${expires}; path=/`;
        },

        get : function(key) {
            const
                name = `${key}=`,
                ca   = document.cookie.split(';');

            for(let i=0; i<ca.length; i++) {
                let c = ca[i];

                while(c.charAt(0) == ' ') {
                    c = c.substring(1,c.length)
                };

                if(c.indexOf(name) == 0) {
                    return c.substring(name.length, c.length);
                }
            }

            return null;
        },

        remove : function(name) {
            document.cookie = `${name}=; Max-Age=-1;`;
        },

        clear : () => {
            const cookies = document.cookie.split(";");
                
            for (let i=0; i<cookies.length; i++) {
                let cookie = cookies[i],
                    eqPos  = cookie.indexOf("="),
                    name   = eqPos > -1 ? cookie.substr(0, eqPos) : cookie;

                document.cookie = `${name}=; expires=Thu, 01 Jan 1970 00:00:00 GMT`;
            }
        }
    };
}());