`use strict`

const express       = require('express')
const bodyParser    = require('body-parser')
const busBoy        = require('express-busboy')
const useragent     = require('express-useragent')
const namedRouter   = require('named-routes')
const route         = require(`${__basedir}/app/router/main`)
const router        = express.Router()
const nmRouter      = new namedRouter()

module.exports = () => {
	// initialise routing
	router.init = () => {
		router.use(bodyParser.json())
		router.use(bodyParser.urlencoded({ extended: true }))
		router.use(useragent.express())
	
		// override response
		router.use((req, res, next) => {
			const json = res.json
	
			next()
		})
	
		nmRouter.extendExpress(router)
	
		busBoy.extend(router, {
			upload : true,
			path   : `${__dirname}/protected/temp/`,
			mimeTypeLimit : [
				'image/jpeg',
				'image/png',
				'application/vnd.ms-excel',
				'application/vnd.ms-powerpoint',
				'application/msword',
				'text/csv',
				'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
				'application/vnd.openxmlformats-officedocument.presentationml.presentation',
				'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
				'application/pdf',
				'application/zip',
				'application/x-rar-compressed',
				'application/x-7z-compressed'
			]
		})

		route(router)
	}

	router.group = (prefix, callback) => {
		let groupRouter = express.Router()
		groupRouter.group = router.group
		nmRouter.extendExpress(groupRouter)
		router.use(prefix, callback(groupRouter))
		return router
	}

	global.router = router

	router.init()

	return router
}