`use strict`
const fs = require(`fs`),
    sequelizeInstances = require(`sequelize`)

module.exports = (path) => {
    return new Promise((resolve, reject) => {
        const sequelize = new sequelizeInstances(env.db.name, env.db.user, env.db.password, {
            host    : env.db.host,
            dialect : env.db.engine,
        })
        sequelize.authenticate().then(() => {
            let db = {}
            if(!path) { path = `${__basedir}/app/schema` }

            fs.readdirSync(path).forEach((file) => {
                let model = require(`${path}/${file}`)

                db[file.replace(/.js|.ts/g, ``)] = model(sequelize)
            })

            global.sequelize = sequelize;
            global.Op = sequelizeInstances.Op;
            global.db = db

            resolve(db)
        }).catch((error) => {
            reject(error)
        })
    })
}