const authorize = require(`${__basedir}/app/middleware/authorize`)

module.exports = (router) => {
    router.get('/', 'provider', [authorize], (req, res) => { models.provider.list(req, res) })
    router.post('/', 'provider', [authorize], (req, res) => { models.provider.add(req, res) })
    router.put('/:id', 'provider', [authorize], (req, res) => { models.provider.edit(req, res) })
    router.delete('/:id', 'provider', [authorize], (req, res) => { models.provider.del(req, res) })
    return router
}