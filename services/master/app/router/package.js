const authorize = require(`${__basedir}/app/middleware/authorize`)

module.exports = (router) => {
    router.get('/', 'package', [authorize], (req, res) => { models.package.list(req, res) })
    router.post('/', 'package', [authorize], (req, res) => { models.package.add(req, res) })
    router.put('/:id', 'package', [authorize], (req, res) => { models.package.edit(req, res) })
    router.delete('/:id', 'package', [authorize], (req, res) => { models.package.del(req, res) })
    return router
}