const authorize = require(`${__basedir}/app/middleware/authorize`)

module.exports = (router) => {
    router.get('/', 'settings', [authorize], (req, res) => { models.settings.view(req, res) })
    router.put('/save-picture', 'settings', [authorize], (req, res) => { models.settings.savePicture(req, res) })
    router.put('/save-personal-data', 'settings', [authorize], (req, res) => { models.settings.savePersonalData(req, res) })
    router.put('/reset-password', 'settings', [authorize], (req, res) => { models.settings.resetPassword(req, res) })
    router.put('/reset-username', 'settings', [authorize], (req, res) => { models.settings.resetUsername(req, res) })
    return router
}