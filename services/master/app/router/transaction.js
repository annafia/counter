const authorize = require(`${__basedir}/app/middleware/authorize`)

module.exports = (router) => {
    router.get('/', 'transaction', [authorize], (req, res) => { models.transaction.list(req, res) })
    router.get('/find-customer', 'transaction', [authorize], (req, res) => { models.transaction.findCustomer(req, res) })
    router.post('/', 'transaction', [authorize], (req, res) => { models.transaction.add(req, res) })
    router.delete('/:id', 'transaction', [authorize], (req, res) => { models.transaction.del(req, res) })
    return router
}