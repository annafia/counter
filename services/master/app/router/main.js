'use strict';
const authenticate  = require(`${__basedir}/app/middleware/authenticate`)
const authorize     = require(`${__basedir}/app/middleware/authorize`)
const settings = require(`./settings`)
const transaction = require(`./transaction`)
const customer = require(`./customer`)
const Package = require(`./package`)
const provider = require(`./provider`)
const account = require(`./account`)

module.exports = (router) => {
    router.post('/authenticate', (req, res) => { authenticate(req, res) })
    router.get('/userinfo', 'userinfo', [authorize], (req, res) => { models.main.userinfo(req, res) })
    router.get('/userinfo/picture', 'userinfo', [authorize], (req, res) => { models.main.picture(req, res) })
    router.post('/logout', (req, res) => { models.main.logout(req, res) })
    
    router.group('/settings', settings)
    router.group('/transaction', transaction)
    router.group('/customer', customer)
    router.group('/package', Package)
    router.group('/provider', provider)
    router.group('/account', account)
}