const authorize = require(`${__basedir}/app/middleware/authorize`)

module.exports = (router) => {
    router.get('/', 'customer', [authorize], (req, res) => { models.customer.list(req, res) })
    router.post('/', 'customer', [authorize], (req, res) => { models.customer.add(req, res) })
    router.put('/:id', 'customer', [authorize], (req, res) => { models.customer.edit(req, res) })
    router.delete('/:id', 'customer', [authorize], (req, res) => { models.customer.del(req, res) })
    return router
}