const authorize = require(`${__basedir}/app/middleware/authorize`)

module.exports = (router) => {
    router.get('/', 'account', [authorize], (req, res) => { models.account.list(req, res) })
    router.post('/', 'account', [authorize], (req, res) => { models.account.add(req, res) })
    router.delete('/:id', 'account', [authorize], (req, res) => { models.account.del(req, res) })
    router.get('/:id', 'account', [authorize], (req, res) => { models.account.view(req, res) })
    router.get('/picture/:id', 'account', [authorize], (req, res) => { models.account.picture(req, res) })
    router.put('/:id/save-picture', 'account', [authorize], (req, res) => { models.account.savePicture(req, res) })
    router.put('/:id/save-personal-data', 'account', [authorize], (req, res) => { models.account.savePersonalData(req, res) })
    router.put('/:id/save-roles', 'account', [authorize], (req, res) => { models.account.saveRoles(req, res) })
    router.put('/:id/reset-password', 'account', [authorize], (req, res) => { models.account.resetPassword(req, res) })
    router.put('/:id/reset-username', 'account', [authorize], (req, res) => { models.account.resetUsername(req, res) })
    return router
}