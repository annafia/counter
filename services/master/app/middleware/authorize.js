`use strict`
const qs = require('querystring')
const { Validator: v } = require('node-input-validator')
const { validate } = require(`${__basedir}/app/helper/helper`)
const sequelize = require('sequelize')

const authorize = async (req, res, next) => {
    req.headers.authorization = req.query.token ? qs.stringify(req.query) : req.headers.authorization
    let validator = new v(req.headers, { authorization: 'required' })
    let matched = await validator.check()
    await validate(res, matched, 'Unauthorized', 403)
    
    const authorization = qs.parse(req.headers.authorization)
    let session = await db.authentication_user_login.findOne({ where: { token: authorization.token, create_at: { [sequelize.Op.gt]: new Date(Date.now() - (60 * 60 * 1000)) }, statusid: 1 }})
    await validate(res, session, 'Unauthorized', 403)

    session.create_at = new Date()
    session = await session.reload()
    let roles = await db.authentication_user_role.findAll({ attributes: ['rolename'], where: { username: session.username, statusid: 1 }, raw: true })
    roles = roles.map(({ rolename }) => rolename)


    let api = await db.authentication_api_role.findOne({
        attributes: ['rolename'], 
        where: { apiname: req.route.name, rolename: { [sequelize.Op.in]: roles }, statusid: 1 }, 
        raw: true
    })
    await validate(res, api, 'Unauthorized', 403)

    req.auth = { username: session.username, roles: roles }
    next()
}

module.exports = authorize