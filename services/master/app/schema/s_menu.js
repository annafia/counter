/**
 * @file        : User.js
 * @author      : Muchamad Mirza <m.mirza@elnusa.co.id>
 * @description : Database menu table mapping
 */

`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define('s_menu', {
        id : {
            type : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true
        },
        menu : {
            type : Sequelize.STRING(25),
            allowNull : false
        },
        s_label_menu_id : {
            type : Sequelize.INTEGER,
            allowNull : false
        },
        parent_id : {
            type : Sequelize.INTEGER,
            allowNull : true
        },
        url : {
            type : Sequelize.STRING(50),
            allowNull : false,
        },
        icons : {
            type : Sequelize.STRING(50),
            allowNull : true
        },
        rolename : {
            type : Sequelize.STRING(50),
            allowNull : true
        },
        rolename_new : {
            type : Sequelize.STRING(50),
            allowNull : true
        },
        sequence : {
            type : Sequelize.INTEGER,
            allowNull : false
        },
        statusid : {
            type : Sequelize.BOOLEAN,
            allowNull : false
        }
    }, {
        schema          : 'counter',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};