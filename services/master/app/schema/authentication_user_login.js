`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`authentication_user_login`, {
        token : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        username : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        create_at : {
            type      : Sequelize.DATE,
            allowNull : false,
            defaultValue: Sequelize.fn('NOW')
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false,
            defaultValue: 1
        },
    }, {
        schema: 'authentication',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};