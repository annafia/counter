`use strict`;

const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define(`s_personal_data`, {
        username : {
            type      : Sequelize.STRING,
            allowNull : false
        },
        name : {
            type: 'citext',
            allowNull : false
        },
        address : {
            type      : Sequelize.STRING,
            allowNull : true
        },
        photo : {
            type      : Sequelize.STRING,
            allowNull : true
        },
        create_at : {
            type      : Sequelize.DATE,
            defaultValue: Sequelize.fn('NOW'),
            allowNull : false
        },
        statusid : {
            type      : Sequelize.SMALLINT,
            allowNull : false
        },
    }, {
        schema: 'public',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};