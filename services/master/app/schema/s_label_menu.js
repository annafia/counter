'use strict';
const Sequelize = require(`sequelize`);

module.exports = (db) => {
    return db.define('s_label_menu', {
        id : {
            type : Sequelize.INTEGER,
            allowNull : false,
            primaryKey: true
        },
        label : {
            type : Sequelize.STRING(15),
            allowNull : false
        },
        statusid : {
            type : Sequelize.BOOLEAN,
            allowNull : false
        },
    }, {
        schema          : 'counter',
        freezeTableName : true,
        createdAt       : false,
        updatedAt       : false
    });
};
