`use strict`
const qs = require('querystring')

module.exports = {
    async menu (rolename = []) {
        const menu = await db.s_menu.findAll({
            attributes : [ 'id', 'menu', 'parent_id', 'url', 'icons', 'sequence', [sequelize.col('s_label_menu.label'), 'label'] ],
            include : [{ model : db.s_label_menu, attributes : [], require : true }],
            where : { rolename : { [Op.overlap] : rolename }, statusid : 1 },
            order : ['sequence'],
            raw: true
        })
    
        let response = [], tempMenu = []
        
        const insertParent = (newMenu, menu) => {
            for (let x in menu) {
                if (menu[x]['id'] === newMenu['parent_id']) {
                    menu[x]['child'] = [
                        ... menu[x]['child'] ? menu[x]['child'] : [],
                        {
                            title       : newMenu.menu,
                            url         : newMenu.url,
                            sequence    : newMenu.sequence,
                            icons       : newMenu.icons
                        }
                    ]
                    break
                }
            }
        }
    
        for (let row of menu) {
            if (row.parent_id) {
                insertParent(row, tempMenu)
            } else {
                tempMenu = [ ... tempMenu, row]
            }
        }
    
        for (let row of tempMenu) {
            response[row.label] = {
                label : row.label,
                menu : [
                    ... response[row.label] ? response[row.label].menu : [],
                    {
                        title : row.menu,
                        icons : row.icons,
                        url : row.url,
                        sequence : row.sequence,
                        child : row.child ? row.child : undefined
                    }
                ]
            }
        }
    
        return { menu: Object.keys(response).map(row => response[row]) }
    },
    async userinfo (req, res) {
        let userinfo = await db.s_personal_data.findOne({
            attributes: ['username', 'name', 'address', 'photo'],
            where: { username: req.auth.username, statusid: 1 },
            raw: true
        })

        userinfo.photo = userinfo.photo ? true : false
        userinfo = { ...userinfo, ... await this.menu(req.auth.roles) }

        res.json(userinfo)
    },
    async picture (req, res) {
        let account = await db.s_personal_data.findOne({
            attributes: [ 'photo' ],
            where: { username: req.auth.username, statusid: 1 },
            raw: true
        })
        res.sendFile(`${__basedir}/protected/account/${account.photo}`)
    },
    async logout(req, res) {
        const authorization = qs.parse(req.body.authorization)
        let session = await db.authentication_user_login.findOne({ where: { token: authorization.token, create_at: { [Op.gt]: new Date(Date.now() - (60 * 60 * 1000)) }, statusid: 1 }})
        if(session) {
            session.statusid = 0
            session = await session.reload()
        }
        res.json({ status: 1 })
    }
}