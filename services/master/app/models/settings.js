`use strict`
const { Validator: v } = require('node-input-validator')
const { validate, commit } = require(`../helper/helper`)
const Sequelize = require('sequelize')
const bcrypt = require('bcryptjs')
const fs = require('fs')

module.exports = {
    async view(req, res) {
        let account = await db.authentication_user.findOne({
            attributes: [
                'username', [sequelize.col('spd.name'), 'name'],
                [sequelize.col('spd.address'), 'address'],
                [sequelize.col('spd.photo'), 'photo']
            ],
            include : [{ model : db.s_personal_data, as: 'spd', attributes : [], where: {statusid: 1}, require : true }],
            where: { username: req.auth.username, statusid: 1 },
            raw: true
        })
        await validate(res, account, 'Account is not found. Please refresh your browser.')

        account.photo = account.photo ? true : false
        res.json({ status: 1, data: { account: account } })
    },
    async picture(req, res) {
        let account = await db.s_personal_data.findOne({
            attributes: [ 'photo' ],
            where: { username: req.auth.username, statusid: 1 },
            raw: true
        })
        await validate(res, account, 'Invalid account')
        res.sendFile(`${__basedir}/protected/account/${account.photo}`)
    },
    async savePicture(req, res) {
        let validator = new v( { ...req.files }, {
            picture: 'required',
            'picture.mimetype': 'in:image/jpg,image/jpeg,image/png'
        })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)

        let find = await db.s_personal_data.findOne({
            attributes: ['id', 'username', 'name', 'address', 'photo'],
            where: { username: req.auth.username, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Account is not found. Please refresh your browser.')

        /** Save File (Image) */
        let filename = req.files.picture.filename.split('.')
        let pictureName = `${find.username}.${filename[filename.length - 1]}`

        fs.rename(req.files.picture.file, `${__basedir}/protected/account/${pictureName}`, async (err) => {
            /** Save Data */
            let save = await sequelize.transaction()
            let deactivate = await db.s_personal_data.update(
                { statusid: 0 },
                { where: { id: find.id, statusid: 1 } }
            )
            let createNew = await db.s_personal_data.create({
                username: find.username,
                name: find.name,
                address: find.address,
                photo: pictureName,
                create_by: req.auth.username,
                statusid: 1
            })

            await commit(save, deactivate && createNew)
            res.json({ status: 1 })
        })
    },
    async savePersonalData(req, res) {
        let validator = new v(req.body, { name: 'required', address: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        let find = await db.s_personal_data.findOne({
            attributes: ['id', 'username', 'name', 'address', 'photo'],
            where: { username: req.auth.username, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Account is not found. Please refresh your browser.')
        
        let save = await sequelize.transaction()
        let deactivate = await db.s_personal_data.update({ statusid: 0 }, { where: { id: find.id, statusid: 1 } })
        let create = await db.s_personal_data.create({
            username: find.username,
            name: req.body.name,
            address: req.body.address,
            photo: find.photo,
            create_by: req.auth.username,
            statusid: 1
        })
        await commit(save, create && deactivate)
        res.json({ status: 1 })
    },
    async resetPassword(req, res) {
        let validator = new v(req.body, {
            password: 'required',
            repassword: 'required|same:repassword',
        })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        let find = await db.authentication_user.findOne({
            attributes: ['id'], where: { username: req.auth.username, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Account is not found. Please refresh your browser.')

        bcrypt.hash(req.body.password, env.app.secret, async (error, password) => {
            let save = await sequelize.transaction()
            
            let deactivate = await db.authentication_user.update({ statusid: 0 }, { where: { username: req.auth.username, statusid: 1 } })
            let create = await db.authentication_user.create({
                username: req.auth.username,
                password: password,
                create_by: req.auth.username,
                statusid: 1
            })
            await commit(save, deactivate && create)

            res.json({ status: 1 })
        })
    },
    async resetUsername(req, res) {
        let validator = new v(req.body, { username: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)

        let find = await db.authentication_user.findOne({
            attributes: ['id', 'username', 'password'], where: { username: req.auth.username, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Account is not found. Please refresh your browser.')
        
        let findUsername = await db.authentication_user.findOne({ 
            attributes: ['id'], 
            where: [{username: req.body.username, id: { [Sequelize.Op.ne]: find.id }, statusid: 1}], 
            raw: true 
        })
        await validate(res, !findUsername, 'Invalid Username. Username has been used by another account.')

        let save = await sequelize.transaction()
        let deactivate = await db.authentication_user.update({ statusid: 0 }, { where: { username: req.auth.username, statusid: 1 } })
        let saveUser = await db.authentication_user.create({
            username: req.body.username,
            password: find.password,
            create_by: req.auth.username,
            statusid: 1
        })
        let savePersonalData = await db.s_personal_data.update(
            { username: req.body.username },
            { where: {username: req.auth.username, statusid: 1} }
        )
        let saveUserRole = await db.authentication_user_role.update(
            { username: req.body.username },
            { where: {username: req.auth.username, statusid: 1} }
        )
        
        await commit(save, saveUser && savePersonalData && saveUserRole && deactivate)
        res.json({ status: 1, username: req.body.username })
    }
}