`use strict`
const { Validator: v } = require('node-input-validator')
const { validate, hashids } = require(`../helper/helper`)

module.exports = {
    async list(req, res) {
        let Package = await db.m_package.findAll({
            attributes: [ 'id', 'package_name', 'provider_id', [sequelize.col('mpv.provider_name'), 'provider_name'], 'purchase', 'sell' ],
            include: [{ model: db.m_provider, as:'mpv', require: true, where: {statusid: 1} }],
            where: { statusid: 1 },
            raw: true
        })
        let provider = await db.m_provider.findAll({
            attributes: ['id', 'provider_name'],
            where: { statusid: 1 },
            raw: true
        })
        Package = hashids.encodeArray(Package, ['id', 'provider_id'])
        provider = hashids.encodeArray(provider, ['id'])
        res.json({ status: 1, data: { packages: Package, provider: provider} })
    },
    async add(req, res) {
        let validator = new v(req.body, { 
            package_name: 'required',
            provider_id: 'required',
            purchase: 'required|numeric',
            sell: 'required|numeric',
        })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)

        req.body.provider_id = hashids.decode(req.body.provider_id)
        let findProvider = await db.m_provider.findOne({ attributes: ['id'], where: [{id: req.body.provider_id, statusid: 1}] })
        await validate(res, findProvider, 'Invalid Provider. Please refresh your browser.')

        let save = await db.m_package.create({
            package_name: req.body.package_name,
            provider_id: req.body.provider_id,
            purchase: req.body.purchase,
            sell: req.body.sell,
            statusid: 1
        })
        await validate(res, save, 'An error occured while saving package\'s data. Please try again.')
        res.json({ status: 1 })
    },
    async edit(req, res) {
        let validator = new v(req.body, { 
            package_name: 'required',
            provider_id: 'required',
            purchase: 'required|numeric',
            sell: 'required|numeric',
        })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        req.params.id = hashids.decode(req.params.id)
        let find = await db.m_package.findOne({
            attributes: ['id'],
            where: { id: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Package is not found')
        
        req.body.provider_id = hashids.decode(req.body.provider_id)
        let findProvider = await db.m_provider.findOne({ attributes: ['id'], where: [{id: req.body.provider_id, statusid: 1}] })
        await validate(res, findProvider, 'Invalid Provider. Please refresh your browser.')
        
        let save = db.m_package.update(
            { 
                package_name: req.body.package_name,
                provider_id: req.body.provider_id,
                purchase: req.body.purchase,
                sell: req.body.sell,
            },
            { where: { id: req.params.id, statusid: 1 } }
        )
        await validate(res, save, 'An error occured while saving package\'s data. Please try again.')
        res.json({ status: 1 })
    },
    async del(req, res) {
        let validator = new v(req.params, { id: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        req.params.id = hashids.decode(req.params.id)
        let find = await db.m_package.findOne({
            attributes: ['id'],
            where: { id: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Package is not found')

        let save = db.m_package.update(
            { statusid: 0 },
            { where: { id: req.params.id, statusid: 1 } }
        )
        
        await validate(res, save, 'An error occured while saving package\'s data. Please try again.')
        res.json({ status: 1 })
    }
}