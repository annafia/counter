`use strict`
const { Validator: v } = require('node-input-validator')
const { validate, hashids } = require(`../helper/helper`)

module.exports = {
    async list(req, res) {
        let provider = await db.m_provider.findAll({
            attributes: ['id', 'provider_name' ],
            where: { statusid: 1 },
            order: ['provider_name'],
            raw: true
        })
        provider = hashids.encodeArray(provider, 'id')
        res.json({ status: 1, data: provider })
    },
    async add(req, res) {
        let validator = new v(req.body, { provider_name: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)

        let save = await db.m_provider.create({
            provider_name: req.body.provider_name,
            statusid: 1
        })

        await validate(res, save, 'An error occured while saving provider\'s data. Please try again.')
        res.json({ status: 1 })
    },
    async edit(req, res) {
        let validator = new v(req.body, { provider_name: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        req.params.id = hashids.decode(req.params.id)
        let find = await db.m_provider.findOne({
            attributes: ['id'],
            where: { id: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Provider is not found')
        
        let save = db.m_provider.update(
            { provider_name: req.body.provider_name },
            { where: { id: req.params.id, statusid: 1 } }
        )
        await validate(res, save, 'An error occured while saving provider\'s data. Please try again.')
        res.json({ status: 1 })
    },
    async del(req, res) {
        let validator = new v(req.params, { id: 'required' })
        let matched = await validator.check()
        await validate(res, matched, validator.errors)
        
        req.params.id = hashids.decode(req.params.id)
        let find = await db.t_transaction.findOne({
            attributes: ['id'],
            where: { id: req.params.id, statusid: 1 },
            raw: true
        })
        await validate(res, find, 'Provider is not found')

        let save = db.t_transaction.update(
            { statusid: 0 },
            { where: { id: req.params.id, statusid: 1 } }
        )
        await validate(res, save, 'An error occured while saving provider\'s data. Please try again.')
        res.json({ status: 1 })
    }
}