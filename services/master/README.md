# master
# There are some screenshots of this apps in folder "screenshots"

<!--- Administrator Account -->
username: m.annafia.super
password: september09
encryption password: bcrypt
authorization token: jwt

<!------------------- How to setup this service -------------------------->
<!--- Needs: -->
1. Node JS
2. Package Manager (such as NPM)
3. PM2
4. Internet Connection
5. Postgres
6. Proxy Pass

<!--- Steps: -->
1. Setup Postgres, There are 3 db schemas which is used in this apps that are authentication, public, & counter.
   You could find thats in folder named "database"
2. Clone this project from Gitlab
3. Install Package for NodeJS "npm i"
4. Setup Proxy Pass. In my case, I use apache as proxy pass, here is the code:
<!--
    <Location "/dealer-managements-api">
        ProxyPass "http://localhost:9600/dealer-managements-api"
    </Location>
-->
5. Run this project from cli using "npm run dev"
6. Don't forget to restart your web server/proxy pass after you make some changes