--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

-- Started on 2020-07-15 09:10:17

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 7 (class 2615 OID 16393)
-- Name: authentication; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA authentication;


ALTER SCHEMA authentication OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 221 (class 1259 OID 16460)
-- Name: authentication_api_role; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_api_role (
    id integer NOT NULL,
    apiname character varying(50) NOT NULL,
    rolename character varying(50) NOT NULL,
    statusid smallint NOT NULL
);


ALTER TABLE authentication.authentication_api_role OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16458)
-- Name: api_role_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.api_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.api_role_id_seq OWNER TO postgres;

--
-- TOC entry 2949 (class 0 OID 0)
-- Dependencies: 220
-- Name: api_role_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.api_role_id_seq OWNED BY authentication.authentication_api_role.id;


--
-- TOC entry 217 (class 1259 OID 16448)
-- Name: authentication_role; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_role (
    id integer NOT NULL,
    rolename character varying(50) NOT NULL,
    roledesc character varying(50) NOT NULL,
    statusid smallint NOT NULL
);


ALTER TABLE authentication.authentication_role OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 16396)
-- Name: authentication_user; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_user (
    id integer NOT NULL,
    username character varying(50) NOT NULL,
    password character varying(200) NOT NULL,
    create_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    create_by character varying(50) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE authentication.authentication_user OWNER TO postgres;

--
-- TOC entry 209 (class 1259 OID 16404)
-- Name: authentication_user_login; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_user_login (
    id integer NOT NULL,
    token character varying(200) NOT NULL,
    username character varying(50) NOT NULL,
    create_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE authentication.authentication_user_login OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16454)
-- Name: authentication_user_role; Type: TABLE; Schema: authentication; Owner: postgres
--

CREATE TABLE authentication.authentication_user_role (
    id integer NOT NULL,
    username character varying(100) NOT NULL,
    rolename character varying(50) NOT NULL,
    create_by character varying(100),
    statusid smallint DEFAULT 1 NOT NULL,
    create_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE authentication.authentication_user_role OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16446)
-- Name: role_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.role_id_seq OWNER TO postgres;

--
-- TOC entry 2950 (class 0 OID 0)
-- Dependencies: 216
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.role_id_seq OWNED BY authentication.authentication_role.id;


--
-- TOC entry 206 (class 1259 OID 16394)
-- Name: user_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.user_id_seq OWNER TO postgres;

--
-- TOC entry 2951 (class 0 OID 0)
-- Dependencies: 206
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.user_id_seq OWNED BY authentication.authentication_user.id;


--
-- TOC entry 208 (class 1259 OID 16402)
-- Name: user_login_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.user_login_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.user_login_id_seq OWNER TO postgres;

--
-- TOC entry 2952 (class 0 OID 0)
-- Dependencies: 208
-- Name: user_login_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.user_login_id_seq OWNED BY authentication.authentication_user_login.id;


--
-- TOC entry 218 (class 1259 OID 16452)
-- Name: user_role_id_seq; Type: SEQUENCE; Schema: authentication; Owner: postgres
--

CREATE SEQUENCE authentication.user_role_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE authentication.user_role_id_seq OWNER TO postgres;

--
-- TOC entry 2953 (class 0 OID 0)
-- Dependencies: 218
-- Name: user_role_id_seq; Type: SEQUENCE OWNED BY; Schema: authentication; Owner: postgres
--

ALTER SEQUENCE authentication.user_role_id_seq OWNED BY authentication.authentication_user_role.id;


--
-- TOC entry 2797 (class 2604 OID 16463)
-- Name: authentication_api_role id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_api_role ALTER COLUMN id SET DEFAULT nextval('authentication.api_role_id_seq'::regclass);


--
-- TOC entry 2793 (class 2604 OID 16451)
-- Name: authentication_role id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_role ALTER COLUMN id SET DEFAULT nextval('authentication.role_id_seq'::regclass);


--
-- TOC entry 2787 (class 2604 OID 16399)
-- Name: authentication_user id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user ALTER COLUMN id SET DEFAULT nextval('authentication.user_id_seq'::regclass);


--
-- TOC entry 2790 (class 2604 OID 16407)
-- Name: authentication_user_login id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_login ALTER COLUMN id SET DEFAULT nextval('authentication.user_login_id_seq'::regclass);


--
-- TOC entry 2794 (class 2604 OID 16457)
-- Name: authentication_user_role id; Type: DEFAULT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_role ALTER COLUMN id SET DEFAULT nextval('authentication.user_role_id_seq'::regclass);


--
-- TOC entry 2943 (class 0 OID 16460)
-- Dependencies: 221
-- Data for Name: authentication_api_role; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

COPY authentication.authentication_api_role (id, apiname, rolename, statusid) FROM stdin;
1	userinfo	admin	1
3	brands	admin	1
2	customer	admin	1
7	settings	super	1
8	settings	admin	1
9	transaction	admin	1
11	transaction	super	1
12	customer	super	1
6	account	super	1
5	package	super	1
4	provider	super	1
13	userinfo	super	1
\.


--
-- TOC entry 2939 (class 0 OID 16448)
-- Dependencies: 217
-- Data for Name: authentication_role; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

COPY authentication.authentication_role (id, rolename, roledesc, statusid) FROM stdin;
1	admin	Administrator	1
2	super	Super Administrator	1
\.


--
-- TOC entry 2935 (class 0 OID 16396)
-- Dependencies: 207
-- Data for Name: authentication_user; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

COPY authentication.authentication_user (id, username, password, create_at, create_by, statusid) FROM stdin;
2	m.annafia	$2b$10$zixCcN86MZ.ZBpbxS6FNQeLjYunDvrxQNx4swD348IySaWJmNkp0O	2020-01-01 21:00:55.588406	m.annafia	1
3	m.annafia.super	$2b$10$zixCcN86MZ.ZBpbxS6FNQeLjYunDvrxQNx4swD348IySaWJmNkp0O	2020-01-01 21:00:55	m.annafia	1
6	annafia.super	$2b$10$zixCcN86MZ.ZBpbxS6FNQeLjYunDvrxQNx4swD348IySaWJmNkp0O	2020-07-14 15:36:04.571271	m.annafia	0
7	annafia.super	$2b$10$zixCcN86MZ.ZBpbxS6FNQez4WXPecvA4iXddtr43rGtuLYsyQKwKe	2020-07-14 20:19:04.743335	m.annafia	0
10	annafia.super.sekalee	$2b$10$zixCcN86MZ.ZBpbxS6FNQez4WXPecvA4iXddtr43rGtuLYsyQKwKe	2020-07-14 20:45:18.368935	m.annafia	1
\.


--
-- TOC entry 2937 (class 0 OID 16404)
-- Dependencies: 209
-- Data for Name: authentication_user_login; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

COPY authentication.authentication_user_login (id, token, username, create_at, statusid) FROM stdin;
2	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTMyNjE2NH0.87dwQUgfIz0NeAnz8Qa8XElTGe0N2CHAWUNe5V2IPtk	m.annafia	2020-02-10 09:16:04.427115	1
3	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTMyNjE4MH0.k_cCCkv75T0OcR9RlDMYVYGqrZIXf6YNeD6QRrna2aU	m.annafia	2020-02-10 09:16:20.434076	1
4	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTMzNTM4MH0.rjnzwIaB0oX2u7iuO2RQ7-TdSJEPDbsls2LyEV6c7Qk	m.annafia	2020-02-10 11:49:40.439613	1
5	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTMzNTQ1OH0.21E0mYCJ58IKj-8CW-FY7pKxT__iKk0OhFNuCu3zH3E	m.annafia	2020-02-10 11:50:58.412635	1
6	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTQ2ODAyMn0.H5tpcXOa-m-xjsDYMCc6pfyBHf11Ju9qQQNewSdGQkk	m.annafia	2020-02-12 00:40:22.105213	1
7	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTQ4ODY3Nn0.yNJgGI_AxmpX7xEr3E_Mh87kQA5IiukWznqnI8ttUa4	m.annafia	2020-02-12 06:24:36.315505	1
8	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTQ5Mjg1NH0.kGm6IDUK7-M4RK-ahPwsZgIcZNnGGBeQru8vSSYOqb8	m.annafia	2020-02-12 07:34:14.738626	1
9	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTQ5ODczOX0.Aor12LOCbcja5LXIeVt-cvdqWj7k9774ImqsFBBdghQ	m.annafia	2020-02-12 09:12:19.31453	1
10	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTUwMjY0N30.u0HxA79uo85LX6aVW5z-AnUpXTthW7ZEgkd8NN4dwYA	m.annafia	2020-02-12 10:17:27.360648	1
11	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTUzNTc4Nn0.D5-RYwMwJCFriL8YZqPRaCT5qVuYjkq4rMbsTgQSl34	m.annafia	2020-02-12 19:29:46.863947	1
12	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTYxODY0Mn0._2jwp5x6RjqnCCFE-kruXJM8fql15Ao-4wHCgUGc1hc	m.annafia	2020-02-13 18:30:42.0314	1
13	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTc4MjA2MH0.T5iQ6--bNjTf70QYE-SBcjCPvQj_h9EXgX6-KJao7YI	m.annafia	2020-02-15 15:54:20.784084	1
14	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTgwOTY0N30.OAzwNEZ31cytVICFEO7GH2u9NtMcMGNmD8LJUvQSKYU	m.annafia	2020-02-15 23:34:07.572592	1
15	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTgyNDkyMn0.klkVAc53V3QhXv7EuoBWOxNrM1NI9Pu-biLExH_W-go	m.annafia	2020-02-16 03:48:42.526526	1
16	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTg0MDA3OH0.3Tdf1zzwTLHUpq2Pj2WIcngNfQkS1Q-_lLdPo5UqS1s	m.annafia	2020-02-16 08:01:18.789881	1
17	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTg0NDc0MH0.mbuT0sKTNIRpsWL6y2T87JA8PdyvbMaxit9XtfQmWVc	m.annafia	2020-02-16 09:19:00.470471	1
18	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTg0NDkwNH0.EQOB5sT8V_SWC1MRPTle1lvleaPWoqvto8VgajL-uXU	m.annafia	2020-02-16 09:21:44.191358	1
19	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4MTg1NjkwN30.a8t2iWKgJ7jswcAy6cYU_ZEODr_nRY0tGPZc9GIxDCs	m.annafia	2020-02-16 12:41:47.54299	1
20	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc1Nzg1OX0.JBA-5Xz1iBZzoSkL15qQ3Lu9Z9IfmFAalszpJ2z0jSs	m.annafia	2020-03-21 02:30:59.185828	1
21	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc3MzIyNn0.8e_tjjZ8cUKGYXUmIYpBgxQQBuDcc7nd7UUX4V7l0LY	m.annafia	2020-03-21 06:47:06.754007	1
22	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc3NzQ0Nn0.4ywfaU3GPIgBiCefMhgUTwvZzFIDxW9Za1AJAUOOMcI	m.annafia	2020-03-21 07:57:26.522735	1
23	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc3NzQ3Mn0.VlVs559sx_u3-V6cKHhqfnJQJ18T34k_I_N7sLgat6A	m.annafia	2020-03-21 07:57:52.128012	1
24	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc3NzUyNH0.pMrpDCVHQnnuv2eB34UpK9W1F-GVUAZkjeAFMuXObWI	m.annafia	2020-03-21 07:58:44.907055	1
25	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc4NDM4NX0.lSP3NTOXF0Pnz_LVGLIsTruVKlvpLgILENSWX4F6M0g	m.annafia	2020-03-21 09:53:05.292814	1
26	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc4NDQyMH0.xFAB-l4pzfuUCgsgs8qZREUvBrD7dNPPsDopAucphiA	m.annafia	2020-03-21 09:53:40.265502	1
27	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc4NDQ2Mn0.mOUPBiQQSUvl1mXquoGV8GnvfMSZRzmVO8WKDx6iyqw	m.annafia	2020-03-21 09:54:22.678285	1
28	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc4ODExOX0.m0qs8aswm69nH-qW-1I88vEdvU-kp9FM1bNk1Nnb9h4	m.annafia	2020-03-21 10:55:19.32734	1
29	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc4OTkyMn0.kkUx7fspND9mb6whqu9NIZBhFLoCPw8hHa8ZkdnvBhI	m.annafia	2020-03-21 11:25:22.713984	1
30	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc5NDc3MX0.7_fZlzunW-yrSxJUlNs1_QfYWpGnZMKJaDyOEur3dsc	m.annafia	2020-03-21 12:46:11.434264	1
31	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDc5ODc4N30.v6iIBd2M-StM6hUUvv5j1Tyghou_BxNRiQ7F69FDx1c	m.annafia	2020-03-21 13:53:07.336043	1
32	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDgwODA5N30.SLm30kkuS2ikqmAw-cZp81IhKY1n3vumEUqvQIHnxzI	m.annafia	2020-03-21 16:28:17.212949	1
33	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDgzNzkxMn0.j5Qj2VHopQdjNCerU9HHS69ZWdOhZg30tIijJCouZlI	m.annafia	2020-03-22 00:45:12.496001	1
34	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDgzODMzNn0.zS_4c7oRL2_dIYz0k791ymCIMMurl7xhU1JK9w-8RRE	m.annafia	2020-03-22 00:52:16.757218	1
35	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDg0ODcwN30.FO0vrgAU3nGtf9rq-BwaylTen93_VHpcGzWp9KzcT5s	m.annafia	2020-03-22 03:45:07.025008	1
36	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU4NDg4NDE0M30.pXl5bpm12bXbn4ptc2SOeBTUiuupk3uy-FnnVhEJefE	m.annafia	2020-03-22 13:35:43.050215	1
1	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU3Nzg5NjQyOH0.TTGGQEoTM4s_gtsdsl79lg2Tpf2NI5yLplSy-L5jpns	m.annafia	2020-01-03 06:22:32.245	1
37	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MTA4ODM0OX0.gwMLtb0PcqGhWn4D6G4yb--NUoug2skolCa1NG-QAAE	m.annafia	2020-06-02 08:59:09.717631	1
38	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MTMyODM3N30.42sOpQBc6GgQNxZS1OsL6_TVnwM1oWXw9NFiRysMvIc	m.annafia	2020-06-05 03:39:37.826719	1
39	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MTMzNTAwMn0.Cb-7fE5Zln4GOHiD3ESEvemA-zTVmNTORZ34z53KkKA	m.annafia	2020-06-05 05:30:02.878974	1
40	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MTMzODE5Mn0.vTGKhIVMYrtVh-IZgn272ake1UrzmfRKHTckZBhBVyI	m.annafia	2020-06-05 06:23:12.265785	1
41	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYTIiLCJpYXQiOjE1OTEzMzgyMTN9.cIUTsdJICYg4yoOOuzHEsMMZI7Euf4bzabjtMINbAmc	m.annafia2	2020-06-05 06:23:33.267571	1
42	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYTIiLCJpYXQiOjE1OTEzMzgyMjd9.0F5mketgivl6zhoFuruWiUc6_RGoq6EJlkGc9VzEzHA	m.annafia2	2020-06-05 06:23:47.707583	1
43	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYTIiLCJpYXQiOjE1OTEzMzgyNTZ9.evlodSkGmunt92iQhApfTfi9WaeYsAqNkc_M5bxWDT8	m.annafia2	2020-06-05 06:24:16.485529	1
44	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYTIiLCJpYXQiOjE1OTEzMzgzNjN9.gN3bOFPVqFnUGMVBFL6kSeJKB3YBRoLeOtiDoO43iAY	m.annafia2	2020-06-05 06:26:03.564228	1
45	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYTIiLCJpYXQiOjE1OTEzMzg0MTh9.q2NDpdrpkoRiCCA-WahNSl6dBQtTTPktBNnpjDMqwXQ	m.annafia2	2020-06-05 06:26:58.05785	1
46	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYTIiLCJpYXQiOjE1OTEzMzg0NDd9.XeUh2cvc3J7ju7C2tzhrF5trOYd7zTxi6kC-vQ8VHfc	m.annafia2	2020-06-05 06:27:27.880888	1
47	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYTIiLCJpYXQiOjE1OTEzMzg0ODJ9.tr-ErU0HFL8c8qN_m25RuUzgFpHi1zBdxEJjkSpbmyM	m.annafia2	2020-06-05 06:28:02.649698	1
48	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjAxODQ3N30.bnZWfYa69FVIH7OSYZAJnPzij1LI9M9t8Sg_xsVYncQ	m.annafia	2020-06-13 03:21:17.352504	1
49	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjE0MDI4OH0.OlIQYD4zoOIro4Q5FDIKgn5OcUFGyBD_eC2XKgpSaO8	m.annafia	2020-06-14 13:11:28.995294	1
50	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjE0MDM0Nn0.h6g8nKFNct9qEQDt_4yO5YH-8vsdalDDXoFopPSVAbs	m.annafia	2020-06-14 13:12:27.001567	1
51	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjE0MDM4NX0.N2elauuLs0-cF54kBjMDXHa6zyFqVKHjrhhMEKttdBk	m.annafia	2020-06-14 13:13:05.27173	1
52	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjE0MDQxN30.8crLQ-izl-q75Xzc0WLuMXMe1VkJfcEkg5XieI6fNOI	m.annafia	2020-06-14 13:13:37.824254	1
53	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjE0MDQ1N30.iRUzaGETYF_1lB8Q8kfGzEuNbFuWXw1fjugiiOOgiPw	m.annafia	2020-06-14 13:14:17.567642	1
54	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjkxNzczNX0.5aA1WgGPYIxIPFANl4n0B56a-muBdYXrLWTn32Ml4hs	m.annafia	2020-06-23 13:08:55.728251	1
55	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjkxNzczOX0.v6kUMWvDsXdbZdKuk6Q2RUvyQrPAiWjNzLBQP41QK24	m.annafia	2020-06-23 13:08:59.027571	1
56	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjkyMjg5NH0.Xmw1gMU3r00CU_VCvymGz8CcvFPRyzGmldhL7U_eVVI	m.annafia	2020-06-23 14:34:54.50831	1
57	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjkyNDQyNH0.3j8ByAPMWOxDiH221qib145lx-i6ygvbqDZ-PSF4wKA	m.annafia	2020-06-23 15:00:24.861978	1
58	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjkyNDQ0Mn0.GuAiQVpuv5eLCVCeE19_01T_qs1y0VgZVTfLpH-9aXE	m.annafia	2020-06-23 15:00:42.552198	1
59	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjkyNDQ4N30.AUn4un0-L3lHRkh23V9P2C-_kYfsjDzV9QDSuf5zfVk	m.annafia	2020-06-23 15:01:27.939595	1
60	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjkyNDUzNH0.fzwA53DCtqKnFrYxNVkrZj7w2LsNvKz6inC0oPwzGWw	m.annafia	2020-06-23 15:02:14.172296	1
61	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjkyNDk0NX0.IIAPNat-JVYNf1cRYhACNP8g3LmOei94lk6qZBB0abU	m.annafia	2020-06-23 15:09:05.336788	1
62	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjkyNTc0M30._QpTfJ1f5fuGzfKfeCEzjsqdqPytzzHAvGv_SvpqcBE	m.annafia	2020-06-23 15:22:23.809539	1
63	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjkyNjAxOX0.EH_MUenW5xCBg5S8FCVmEQJ7L_sCm4vWTTJ1ghJPmUY	m.annafia	2020-06-23 15:26:59.287384	1
64	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MjkzMDc2MX0.mwN08PMX0cD4QLtYLh65gYPqIlO4Fxb6bZdlwDFiYw8	m.annafia	2020-06-23 16:46:01.456407	1
65	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzAxNjQ0NH0.9S9goFrWdu1i6aitifCfJlqye7iRcx-7nzRITm1hNIQ	m.annafia	2020-06-24 16:34:04.908496	1
66	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzA5OTY2OH0.CnrIfxM8UaWfxY1RpMc8r1OuM32uskgwd2XZ2dCdIro	m.annafia	2020-06-25 15:41:08.404458	1
67	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0NDg3NH0.Da5CABA4V8Tkmtaa_aG450vICOWOIG2Ccxh-Cawj1ps	m.annafia	2020-06-28 11:47:54.816633	1
68	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0NDkwOH0.tJitM_EZABUndio3VoJIdmLig21_BiymkLmXBnTeA3g	m.annafia	2020-06-28 11:48:28.619964	1
69	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0NDk3MH0.WTQvX_o5MUap-9FBQebuha5BBvQbPLmKPMu2i49Yfjo	m.annafia	2020-06-28 11:49:30.886168	1
70	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0NTE5NH0.vMMayGC9NNxDFM1nJ1ewcMLDLrQYqKr7sUK9iUCxdTc	m.annafia	2020-06-28 11:53:14.558042	1
71	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0NTUxN30.niM7ojW-ZqObP6oyVC3bnBzVKiFAWaTZDvgZrjFu0Gc	m.annafia	2020-06-28 11:58:37.785125	1
72	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0NTU4MH0.NMGg7aD18ps-mcINbQlqCg0vyvdkPIbdVIlLVhk2JO0	m.annafia	2020-06-28 11:59:40.391142	1
73	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0NTYyMn0.YUpre-Zp0mEq7f1weCybbxa8bIMLAUL7tq_zggkyvJw	m.annafia	2020-06-28 12:00:22.311279	1
74	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0NTY2OH0.Py1rHynqMuF91HPCzdzAfG8izmtrfx_q71H5gWaKf4g	m.annafia	2020-06-28 12:01:08.18451	1
75	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0NTcyN30.rnKQwgmCJ8jmIOGT_Hg_exhUYw9NC429cArST1K6YOE	m.annafia	2020-06-28 12:02:07.402264	1
76	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0NTg3Mn0.hTIfEhKcD0Fnp9WaITlZGYp1oIqnuO5fFeHXLwWRWiI	m.annafia	2020-06-28 12:04:32.880047	1
77	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0NTk1M30.XPO8ZyMBn5kWqqHVszFoXLzC2cfHL0o1HUmzjdLj-eA	m.annafia	2020-06-28 12:05:53.843881	1
78	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0NjA2MX0.10nbhYwcca6GDpW51B1ddTp7xcDBZTmBRdGbKvA1l8o	m.annafia	2020-06-28 12:07:41.156078	1
79	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0NzQ5MH0.zARuNftaFIgTqMwSgrI5peAQ-9NG9H6BFxW3NfRo8xk	m.annafia	2020-06-28 12:31:30.795791	1
80	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0Nzc3MH0.90P7eC1TONyxKQO1-hfxPh8rw4JEghhqQNmsYa-nY_o	m.annafia	2020-06-28 12:36:10.638648	1
81	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0ODE5M30.9b6At_4wfaJQoG2XMfepcgNZC_asJWT7Op7GR57htrQ	m.annafia	2020-06-28 12:43:13.122038	1
82	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0ODE5N30.ve24wGFp6IJ1oVjWOR8tjcL3R2AlnWaK__BDLRRdu8w	m.annafia	2020-06-28 12:43:17.414963	1
83	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0ODI3OX0.nb3goYVK08uwqZuwgjxZPkdoXnPzRxCIW2eDCQ6a3bo	m.annafia	2020-06-28 12:44:39.431164	1
84	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0ODc1OX0.gpklIOhsexPi5FTDIHZ_5QRwLE0VkKHgNxwCOVh2xY4	m.annafia	2020-06-28 12:52:39.042278	1
85	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0ODc3OH0.qUsyaQ7R2lClFdcRq2lJxXpiVvZ3R1XuRSvX5Tgm0qo	m.annafia	2020-06-28 12:52:58.714867	1
86	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0ODgxNX0.Fu9cjTBfMvGwwNO3d3fI0oVCuNJFKBP76Z4BlDkQzK4	m.annafia	2020-06-28 12:53:35.504755	1
87	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0ODg2MH0.TN7jPP8ZnmyWA0kEMqFzExYriLlsbyPagqZpKlh3fpc	m.annafia	2020-06-28 12:54:20.511432	1
88	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0ODk0OH0.ZVINTpxLdIZ0NcWa0P1U6i_AItnS4pps037chNmnMAs	m.annafia	2020-06-28 12:55:48.970479	1
89	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0ODk4Nn0.rFpINmliSYOVA0jfkcFG_YE871dHeKoUbPD_QgNCfdM	m.annafia	2020-06-28 12:56:26.638059	1
90	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0OTAyMX0.b27jv-2UYTTD4iEqPAHSr2YfFwYsEj5pKqACkRMQP7c	m.annafia	2020-06-28 12:57:01.311448	1
91	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0OTA2Mn0.Fx3CzQGHbyo3vJ_9lG9euQPFekErAZV4-pC3iKNIxhw	m.annafia	2020-06-28 12:57:42.228008	1
92	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0OTEyN30.t54bu5YAL1L0sXCu2KmuUHN-RIQjmoKGGxGV_bluKks	m.annafia	2020-06-28 12:58:47.464545	1
93	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0OTIyN30.lw83i7TDYP441iMMCJvDcknqoQXCzIC1tjEMhYg7Rk8	m.annafia	2020-06-28 13:00:27.851923	1
94	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0OTI1Mn0.sD0hSbosco_X8u-MvipCRUMMH0ajZDqvdrjezYm8JKE	m.annafia	2020-06-28 13:00:52.620958	1
95	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0OTI4Nn0.wvczDI8C822Dl1D3kNuc3DBbMPyITrGChlT-UxnzaXE	m.annafia	2020-06-28 13:01:26.500516	1
96	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0OTMyNH0.z1rtWACAJt18xc3MXKJXC8hrmemcARzEzjnRCGSEnLs	m.annafia	2020-06-28 13:02:04.603223	1
97	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0OTM0Nn0.enIm-jh2PVOwSBJWLivtQ02QGJlcFbzgB23XL8gj5dw	m.annafia	2020-06-28 13:02:26.866713	1
98	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0OTM4MX0.h0-NpbZrxtOZJpEZU1QltSvbpkzu9AXSJBUXaEEPZIM	m.annafia	2020-06-28 13:03:01.186142	1
99	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0OTQxMn0.hJVDKwSZmjEgP6c52S9t5LGz4YHJ2NGEv5-y31jpW00	m.annafia	2020-06-28 13:03:32.301666	1
100	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM0OTQ4Nn0.TUnFpx8ipq3sgDmBAwUADAbwZLrPTEOA7A1etJaRfoA	m.annafia	2020-06-28 13:04:46.713285	1
101	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM1MDYzNn0.iaFIQw7HBK_Uu0eovdlllgWhoJZYqgUjOepD-liIftg	m.annafia	2020-06-28 13:23:56.979077	1
102	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM1MDY2MH0.sP7jV4JWcSusONS9uB3iaZAwO3NJ_TTHDbMma6jmWVY	m.annafia	2020-06-28 13:24:20.382321	1
103	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM1MDc0NX0.SHokzptbh9z251NHhW2ezA0cG-6U_TWVGBKpDXqCMKo	m.annafia	2020-06-28 13:25:45.495236	1
104	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM1MDgxOX0.KFcxrfO5gBxkNq3oBaBFzZf8RjrxHRxjU8JzLpsmC6I	m.annafia	2020-06-28 13:26:59.310058	1
105	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM1Mzc5MH0.aybBO6VU_bz4vWNhDT4Q61kFSLZJQyWLysuisc6Sv9M	m.annafia	2020-06-28 14:16:30.668831	1
106	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzM1NjM2Mn0.C3quHZiChZ0ebyzyIHOWmneCM-uASJQPliq-VYanh6Q	m.annafia	2020-06-28 14:59:22.096479	1
107	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzQyODY1OH0.jPnpg4P6Xtd_RInpvc08bySPnKzbmE_r9A1btMq_8yI	m.annafia	2020-06-29 11:04:18.171455	1
108	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzQyODY3NX0.qJbGbtFt4QzNLukH8xsKVdlqH9pGey6aISBc_v83c9U	m.annafia	2020-06-29 11:04:35.307828	1
109	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzQzNDA5Mn0.AVDis3bpkrsJpMkYfCc-bigjjlYtyomvobYLXeTnj2s	m.annafia	2020-06-29 12:34:52.064106	1
110	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzQ0NDEwNX0.B3JZ8caJFLYjoj01T6e40utds949eyHiAF3OKD-XrUA	m.annafia	2020-06-29 15:21:45.115798	1
111	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzU2ODg1OX0.jRHaqJ5EqBYN9S3yKixR-X2jme5Uh3OXHnmArwq0Jfc	m.annafia	2020-07-01 02:00:59.793778	1
112	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzU2ODg4MX0.yDVzhn2ngByjSI3qn0MUTOtzrnGse2eMWMCOCra1rVY	m.annafia	2020-07-01 02:01:21.976847	1
113	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzU2ODg5M30.EzCWakKwOvaecewRyNsQaA_GL3eOwyjeQcp0b1L8AMA	m.annafia	2020-07-01 02:01:33.251342	1
114	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzU2ODkzOX0.54b_emzTusQ3tmI70z8TXVxRXdM8BWp5dw6V3DVk4Ig	m.annafia	2020-07-01 02:02:19.975716	1
115	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzU2OTAxOX0.IsiodOPqIbiW0o783gDLB99Mi8OhTzzittzOmsk1W1g	m.annafia	2020-07-01 02:03:39.725962	1
116	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzU2OTIzNn0.PM43BMs0R36cgJAkwW9w_Q9m14Jy0sJXgk2eKyy3npA	m.annafia	2020-07-01 02:07:16.77815	1
117	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzU4NjA2NX0.Q7UQ4szApg2GPeAMrgFWzMCurXFgmXM10ygXZ5lJUyU	m.annafia	2020-07-01 06:47:45.248739	1
118	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzU4NjE1Mn0.6Njz91ZvrveD1ndNFNYHuxCaGPFLYXPm1EooPlPlBf4	m.annafia	2020-07-01 06:49:12.994106	1
119	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzY2OTEwMn0.GzWEuz_-f7i_zefVL7uAtiIHLq04KKpQ_9HFeQoiHc8	m.annafia	2020-07-02 05:51:42.878437	1
120	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5MzY3MTMwMH0.UR2ovGALkuq3otutzAlyyV_J9Gm2hHQz2BXClXqf27E	m.annafia	2020-07-02 06:28:20.55655	1
121	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5Mzk0MjkyOH0.EF5N3og41g250Y9PRDvCYay-Ucz0CngxUrbKgd6hTDQ	m.annafia	2020-07-05 09:55:28.915073	1
122	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5Mzk0Njk3NX0.Z5hLLLdy-ukGdOLRGgWWVp7l0x4to8TBj4xdV0ThZTg	m.annafia	2020-07-05 11:02:55.795793	1
123	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDM3MjIzNn0.ej-hQ_f67esYImOKmkN-VloH2nex7hk0ZTKb62qLLg8	m.annafia	2020-07-10 09:10:36.552183	1
124	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDQ1MjM1Mn0.oHzRpySZMEwe4n2awrM4XY3eE-Upzoac6bzyhFNUFEM	m.annafia	2020-07-11 07:25:52.887439	1
125	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDU0MjUzNH0.dB-HnwSgWS7ol46Dz2Y6f0eCRq5o-CskIu0CsS2tTmk	m.annafia	2020-07-12 08:28:54.468341	1
126	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDU0NTk5Mn0.wIx4ZpW28eCAa-2DZonFCG3Yv38Tn0YCep9HWRnAePk	m.annafia	2020-07-12 09:26:32.756881	1
127	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDU1ODk5Nn0.AM_jm6RzaFG-p4uMthax0-Llkq_kAtKrFnaERwUCc_o	m.annafia	2020-07-12 13:03:16.67571	1
128	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDU2Mjg1NH0.u63ma6bpXrXOWU2E8ymRUn5p2O7191wq2-7vw1vidFs	m.annafia	2020-07-12 14:07:34.01766	1
129	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDY0MDM2NX0.1M2lLlYD5CivVpOM9nvmfU7J3kEZEClM3iDJaAWWSVw	m.annafia	2020-07-13 11:39:25.497114	1
130	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDY1NDkxOX0.8JesYVON0Ilrv0q5qI94Z5P3EvwRoX207PTsrM9Ia4g	m.annafia	2020-07-13 15:41:59.743721	1
131	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDY1NjU4NX0.D45n8s4FlSeGN8fyWVrGy3Y2x5x5fps8j3ry6zoTLAA	m.annafia	2020-07-13 16:09:45.676397	1
132	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDY1NjYxM30.WGRg7fW-P1RI1aNSgMW_VoqkRprexJcPGDcRvcvIKeA	m.annafia	2020-07-13 16:10:13.159827	1
133	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcwMTc4N30.6ZohbnzJ3XwVg3LwReRSxe4gATAX62cg5CZIiGCYLOY	m.annafia	2020-07-14 04:43:07.526405	1
134	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcwNDA5N30.UyxtP_w3tJiL8L9GCEdZLINJedH4UsXaU4cRONbHGrg	m.annafia	2020-07-14 05:21:37.913242	1
135	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcwNDExMH0.Wp5mWTYRrsuLficIMORumUA1edPJrAs7EBLELOEGDPE	m.annafia	2020-07-14 05:21:50.15527	1
136	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcwNDE1MH0.MueEehFHKX_Y5ktVHDxUBK9IMnBmrAAdFwIgfFoDvgQ	m.annafia	2020-07-14 05:22:30.022538	1
137	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcwOTczNH0.HAE8G0UiZvHDbooRe76AlrX3_uJNygADFv2deCUrYGs	m.annafia	2020-07-14 06:55:34.875916	1
138	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcxMDE1MX0.7m6uMvGYBWtVT9cmxXPsD-59pd6Kzn_vuHr7GX8f0fY	m.annafia	2020-07-14 07:02:31.427666	1
139	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcxMDE3OX0.5Cnyb3HeomGKvoIHbr_7JdD_dwVlEj24eI2rAJ__1Bk	m.annafia	2020-07-14 07:02:59.282763	1
140	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcxMzkyOH0.oIsexx18Khz3EPSb4qjTOfu9JjmEQXvED1wTUjZK8KA	m.annafia	2020-07-14 08:05:28.516907	1
141	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcxNzY0OX0.l-E_j64_JZ58xlyBe_Lur1p1hR_cd_ZXiOwRfEkdQXU	m.annafia	2020-07-14 09:07:29.822582	1
142	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcxOTM5N30.M5XDgmHucxnkteSuqJbE7XwuR-N2Zx1BYKRTZ2coK74	m.annafia	2020-07-14 09:36:37.224508	1
143	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcxOTQyNX0.bPCsgqK__xFQV0OLGHUhJ9B6-5J2CidnjbNKCuTo5gY	m.annafia	2020-07-14 09:37:05.501614	1
144	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcyMjIzMn0.uoesCyu9KafWMOY3p5frk3vfkbNXOP9Si69AbDmzqj4	m.annafia	2020-07-14 10:23:52.585048	1
145	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcyNjMyNH0.rOOXwfeQZFGVh8kfvNCbSPviXmRIMeUaIhkxo9aO7To	m.annafia	2020-07-14 11:32:04.699356	1
146	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDcyOTk1OX0.4n9CP3LGO4tX3zHW6OXvlp4n4sJnSpyDcCm3KpgZ35k	m.annafia	2020-07-14 12:32:39.260656	1
147	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDczNjA0MH0.AJP_uNWs7hP-3Z3dod_sktoGlAZGzdNoi6I450Xs1tQ	m.annafia	2020-07-14 14:14:00.821915	1
148	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDc0MDY2Mn0.9hoovyP5CXepIEGXTs_1UurAWVXAZQktWFd6TTNqpaE	m.annafia	2020-07-14 15:31:02.16413	1
149	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDc0NDU5OH0.O_Q4fT9VGjsYQCpwaaiknyHRbbsnvd4rpQEUAZHyhWs	m.annafia	2020-07-14 16:36:38.288938	1
150	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDc0OTExNX0.lKEOIEsnJQ2MUtFqdYwXxZrt7ZTLugOWkuLPz7Iyrpk	m.annafia	2020-07-14 17:51:55.77158	1
151	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDc1Mjc1MH0.yJJ2g8o6YvDM5D4q1Igyp2_cxNqBNhavEXvXb3V61aE	m.annafia	2020-07-14 18:52:30.352006	1
152	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDc1NjQyMX0.XA0AglWiMziaSoPCM10NtMoT8cJe3Ov6yHCqS0mbcew	m.annafia	2020-07-14 19:53:41.282777	1
153	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDc2MDA1NH0.YS4v-QJg0RP46lbFKdK7YTzL1MViwqefcWv_ftQqcho	m.annafia	2020-07-14 20:54:14.034411	1
154	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDc2NDg4MX0.--qbHuZuhnWofk1DymMRobwNQWXha3BYXcObXKRzmhg	m.annafia	2020-07-14 22:14:41.137911	1
155	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDc3MzYwOX0.YG1ouOfEZvOYVgIRw7K949cveEWF_G2U7MZmddQXsjk	m.annafia	2020-07-15 00:40:09.495182	1
156	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDc3NzI0NX0.mI1D5kQbHTUYQNNEMtTQQLPQNqSRX53mzHioFLXTX78	m.annafia	2020-07-15 01:40:45.966916	1
157	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYSIsImlhdCI6MTU5NDc3NzU5MX0._vR729kixRA3jzBmFG7R-Jx7zfivX4XXKyRZULjTrto	m.annafia	2020-07-15 01:46:31.525035	1
158	eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6Im0uYW5uYWZpYS5zdXBlciIsImlhdCI6MTU5NDc3NzYxNX0.uniab0rm8CEoA9sWnuASla3DCJ_O0kvY3GY_8W0_mGk	m.annafia.super	2020-07-15 01:46:55.785848	1
\.


--
-- TOC entry 2941 (class 0 OID 16454)
-- Dependencies: 219
-- Data for Name: authentication_user_role; Type: TABLE DATA; Schema: authentication; Owner: postgres
--

COPY authentication.authentication_user_role (id, username, rolename, create_by, statusid, create_at) FROM stdin;
1	m.annafia	admin	m.annafia	1	2020-02-15 23:12:32.452727
2	m.annafia.super	super	m.annafia	1	2020-02-15 23:12:32
15	annafia.super	admin	m.annafia	0	2020-07-14 20:00:05.424267
16	annafia.super	admin	m.annafia	0	2020-07-14 20:01:36.624032
17	annafia.super.sekalee	admin	m.annafia	1	2020-07-14 20:01:43.163148
18	annafia.super.sekalee	super	m.annafia	1	2020-07-14 20:01:43.163148
\.


--
-- TOC entry 2954 (class 0 OID 0)
-- Dependencies: 220
-- Name: api_role_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.api_role_id_seq', 13, true);


--
-- TOC entry 2955 (class 0 OID 0)
-- Dependencies: 216
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.role_id_seq', 2, true);


--
-- TOC entry 2956 (class 0 OID 0)
-- Dependencies: 206
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.user_id_seq', 10, true);


--
-- TOC entry 2957 (class 0 OID 0)
-- Dependencies: 208
-- Name: user_login_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.user_login_id_seq', 158, true);


--
-- TOC entry 2958 (class 0 OID 0)
-- Dependencies: 218
-- Name: user_role_id_seq; Type: SEQUENCE SET; Schema: authentication; Owner: postgres
--

SELECT pg_catalog.setval('authentication.user_role_id_seq', 18, true);


--
-- TOC entry 2807 (class 2606 OID 24644)
-- Name: authentication_api_role api_role_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_api_role
    ADD CONSTRAINT api_role_pk PRIMARY KEY (id);


--
-- TOC entry 2803 (class 2606 OID 24646)
-- Name: authentication_role role_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_role
    ADD CONSTRAINT role_pk PRIMARY KEY (id);


--
-- TOC entry 2801 (class 2606 OID 16409)
-- Name: authentication_user_login user_login_pkey; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_login
    ADD CONSTRAINT user_login_pkey PRIMARY KEY (id);


--
-- TOC entry 2799 (class 2606 OID 16401)
-- Name: authentication_user user_pkey; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user
    ADD CONSTRAINT user_pkey PRIMARY KEY (id);


--
-- TOC entry 2805 (class 2606 OID 24642)
-- Name: authentication_user_role user_role_pk; Type: CONSTRAINT; Schema: authentication; Owner: postgres
--

ALTER TABLE ONLY authentication.authentication_user_role
    ADD CONSTRAINT user_role_pk PRIMARY KEY (id);


-- Completed on 2020-07-15 09:10:18

--
-- PostgreSQL database dump complete
--

