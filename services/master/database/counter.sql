--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

-- Started on 2020-07-15 09:11:41

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 10 (class 2615 OID 32876)
-- Name: counter; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA counter;


ALTER SCHEMA counter OWNER TO postgres;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 233 (class 1259 OID 32900)
-- Name: t_customer; Type: TABLE; Schema: counter; Owner: postgres
--

CREATE TABLE counter.t_customer (
    id integer NOT NULL,
    name character varying(200) NOT NULL,
    email character varying(100) NOT NULL,
    address character varying(255) NOT NULL,
    create_at timestamp without time zone NOT NULL,
    create_by character varying(100) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL,
    phone character varying(50) NOT NULL
);


ALTER TABLE counter.t_customer OWNER TO postgres;

--
-- TOC entry 232 (class 1259 OID 32898)
-- Name: customer_id_seq; Type: SEQUENCE; Schema: counter; Owner: postgres
--

CREATE SEQUENCE counter.customer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE counter.customer_id_seq OWNER TO postgres;

--
-- TOC entry 2955 (class 0 OID 0)
-- Dependencies: 232
-- Name: customer_id_seq; Type: SEQUENCE OWNED BY; Schema: counter; Owner: postgres
--

ALTER SEQUENCE counter.customer_id_seq OWNED BY counter.t_customer.id;


--
-- TOC entry 235 (class 1259 OID 32910)
-- Name: m_package; Type: TABLE; Schema: counter; Owner: postgres
--

CREATE TABLE counter.m_package (
    id integer NOT NULL,
    package_name character varying(50) NOT NULL,
    purchase integer NOT NULL,
    sell integer NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL,
    provider_id integer NOT NULL
);


ALTER TABLE counter.m_package OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 32919)
-- Name: m_provider; Type: TABLE; Schema: counter; Owner: postgres
--

CREATE TABLE counter.m_provider (
    id integer NOT NULL,
    provider_name character varying(50) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE counter.m_provider OWNER TO postgres;

--
-- TOC entry 236 (class 1259 OID 32917)
-- Name: m_provider_id_seq; Type: SEQUENCE; Schema: counter; Owner: postgres
--

CREATE SEQUENCE counter.m_provider_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE counter.m_provider_id_seq OWNER TO postgres;

--
-- TOC entry 2956 (class 0 OID 0)
-- Dependencies: 236
-- Name: m_provider_id_seq; Type: SEQUENCE OWNED BY; Schema: counter; Owner: postgres
--

ALTER SEQUENCE counter.m_provider_id_seq OWNED BY counter.m_provider.id;


--
-- TOC entry 234 (class 1259 OID 32908)
-- Name: package_id_seq; Type: SEQUENCE; Schema: counter; Owner: postgres
--

CREATE SEQUENCE counter.package_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE counter.package_id_seq OWNER TO postgres;

--
-- TOC entry 2957 (class 0 OID 0)
-- Dependencies: 234
-- Name: package_id_seq; Type: SEQUENCE OWNED BY; Schema: counter; Owner: postgres
--

ALTER SEQUENCE counter.package_id_seq OWNED BY counter.m_package.id;


--
-- TOC entry 229 (class 1259 OID 32879)
-- Name: s_label_menu; Type: TABLE; Schema: counter; Owner: postgres
--

CREATE TABLE counter.s_label_menu (
    id integer NOT NULL,
    label character varying(50) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE counter.s_label_menu OWNER TO postgres;

--
-- TOC entry 228 (class 1259 OID 32877)
-- Name: s_label_menu_id_seq; Type: SEQUENCE; Schema: counter; Owner: postgres
--

CREATE SEQUENCE counter.s_label_menu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE counter.s_label_menu_id_seq OWNER TO postgres;

--
-- TOC entry 2958 (class 0 OID 0)
-- Dependencies: 228
-- Name: s_label_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: counter; Owner: postgres
--

ALTER SEQUENCE counter.s_label_menu_id_seq OWNED BY counter.s_label_menu.id;


--
-- TOC entry 231 (class 1259 OID 32886)
-- Name: s_menu; Type: TABLE; Schema: counter; Owner: postgres
--

CREATE TABLE counter.s_menu (
    id integer NOT NULL,
    menu character varying(25) NOT NULL,
    label_id integer NOT NULL,
    parent_id integer,
    url character varying(50),
    icons character varying(50),
    statusid smallint DEFAULT 1 NOT NULL,
    sequence integer NOT NULL,
    rolename text[]
);


ALTER TABLE counter.s_menu OWNER TO postgres;

--
-- TOC entry 230 (class 1259 OID 32884)
-- Name: s_menu_id_seq; Type: SEQUENCE; Schema: counter; Owner: postgres
--

CREATE SEQUENCE counter.s_menu_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE counter.s_menu_id_seq OWNER TO postgres;

--
-- TOC entry 2959 (class 0 OID 0)
-- Dependencies: 230
-- Name: s_menu_id_seq; Type: SEQUENCE OWNED BY; Schema: counter; Owner: postgres
--

ALTER SEQUENCE counter.s_menu_id_seq OWNED BY counter.s_menu.id;


--
-- TOC entry 241 (class 1259 OID 32940)
-- Name: t_transaction; Type: TABLE; Schema: counter; Owner: postgres
--

CREATE TABLE counter.t_transaction (
    id integer NOT NULL,
    customer_id character varying(100) NOT NULL,
    package_id integer NOT NULL,
    transaction_date timestamp without time zone NOT NULL,
    create_at timestamp without time zone NOT NULL,
    create_by character varying(100) NOT NULL,
    statusid smallint DEFAULT 1 NOT NULL
);


ALTER TABLE counter.t_transaction OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 32938)
-- Name: t_transaction_id_seq; Type: SEQUENCE; Schema: counter; Owner: postgres
--

CREATE SEQUENCE counter.t_transaction_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE counter.t_transaction_id_seq OWNER TO postgres;

--
-- TOC entry 2960 (class 0 OID 0)
-- Dependencies: 240
-- Name: t_transaction_id_seq; Type: SEQUENCE OWNED BY; Schema: counter; Owner: postgres
--

ALTER SEQUENCE counter.t_transaction_id_seq OWNED BY counter.t_transaction.id;


--
-- TOC entry 2794 (class 2604 OID 32913)
-- Name: m_package id; Type: DEFAULT; Schema: counter; Owner: postgres
--

ALTER TABLE ONLY counter.m_package ALTER COLUMN id SET DEFAULT nextval('counter.package_id_seq'::regclass);


--
-- TOC entry 2796 (class 2604 OID 32922)
-- Name: m_provider id; Type: DEFAULT; Schema: counter; Owner: postgres
--

ALTER TABLE ONLY counter.m_provider ALTER COLUMN id SET DEFAULT nextval('counter.m_provider_id_seq'::regclass);


--
-- TOC entry 2788 (class 2604 OID 32882)
-- Name: s_label_menu id; Type: DEFAULT; Schema: counter; Owner: postgres
--

ALTER TABLE ONLY counter.s_label_menu ALTER COLUMN id SET DEFAULT nextval('counter.s_label_menu_id_seq'::regclass);


--
-- TOC entry 2790 (class 2604 OID 32889)
-- Name: s_menu id; Type: DEFAULT; Schema: counter; Owner: postgres
--

ALTER TABLE ONLY counter.s_menu ALTER COLUMN id SET DEFAULT nextval('counter.s_menu_id_seq'::regclass);


--
-- TOC entry 2792 (class 2604 OID 32903)
-- Name: t_customer id; Type: DEFAULT; Schema: counter; Owner: postgres
--

ALTER TABLE ONLY counter.t_customer ALTER COLUMN id SET DEFAULT nextval('counter.customer_id_seq'::regclass);


--
-- TOC entry 2798 (class 2604 OID 32943)
-- Name: t_transaction id; Type: DEFAULT; Schema: counter; Owner: postgres
--

ALTER TABLE ONLY counter.t_transaction ALTER COLUMN id SET DEFAULT nextval('counter.t_transaction_id_seq'::regclass);


--
-- TOC entry 2945 (class 0 OID 32910)
-- Dependencies: 235
-- Data for Name: m_package; Type: TABLE DATA; Schema: counter; Owner: postgres
--

COPY counter.m_package (id, package_name, purchase, sell, statusid, provider_id) FROM stdin;
2	Paket Malam Minggu Miko	50000	60000	1	2
1	Paket Begadang 20GB Puas	70000	100000	1	5
\.


--
-- TOC entry 2947 (class 0 OID 32919)
-- Dependencies: 237
-- Data for Name: m_provider; Type: TABLE DATA; Schema: counter; Owner: postgres
--

COPY counter.m_provider (id, provider_name, statusid) FROM stdin;
2	Telkomsel	1
3	Axis	1
5	3 (Three)	1
1	Indosat	1
4	XL	1
\.


--
-- TOC entry 2939 (class 0 OID 32879)
-- Dependencies: 229
-- Data for Name: s_label_menu; Type: TABLE DATA; Schema: counter; Owner: postgres
--

COPY counter.s_label_menu (id, label, statusid) FROM stdin;
1	Administrator	1
2	Master Data	1
3	Personal	1
\.


--
-- TOC entry 2941 (class 0 OID 32886)
-- Dependencies: 231
-- Data for Name: s_menu; Type: TABLE DATA; Schema: counter; Owner: postgres
--

COPY counter.s_menu (id, menu, label_id, parent_id, url, icons, statusid, sequence, rolename) FROM stdin;
6	Settings	3	\N	#/settings	mdi mdi-settings	1	6	{admin,super}
5	Provider	2	\N	#/provider	mdi mdi-cloud-check	1	3	{super}
2	Package	2	\N	#/package	mdi mdi-tag-multiple	1	4	{super}
4	Account	2	\N	#/account	mdi mdi-account-multiple	1	5	{super}
1	Transaction	1	\N	#/transaction	mdi mdi-database-settings	1	2	{admin,super}
3	Customer	1	\N	#/customer	mdi mdi-account-check	1	1	{admin,super}
\.


--
-- TOC entry 2943 (class 0 OID 32900)
-- Dependencies: 233
-- Data for Name: t_customer; Type: TABLE DATA; Schema: counter; Owner: postgres
--

COPY counter.t_customer (id, name, email, address, create_at, create_by, statusid, phone) FROM stdin;
1	Mochammad Annafia	m.annafia@gmail.com	Pandaan	2020-07-14 18:10:54.488834	m.annafia	0	089888212888
3	Mochammad Annafia Oktafian	m.annafia@gmail.co.id	Pandaan, Pasuruan	2020-07-14 18:16:24.363311	m.annafia	0	089888212881
2	Mochammad Annafia Oktafian	m.annafia@gmail.co.id	Pandaan, Pasuruan	2020-07-14 18:12:33.64199	m.annafia	1	089888212881
\.


--
-- TOC entry 2949 (class 0 OID 32940)
-- Dependencies: 241
-- Data for Name: t_transaction; Type: TABLE DATA; Schema: counter; Owner: postgres
--

COPY counter.t_transaction (id, customer_id, package_id, transaction_date, create_at, create_by, statusid) FROM stdin;
2	m.annafia@gmail.co.id	2	2020-07-15 00:00:00	2020-07-15 01:15:03.152866	m.annafia	1
\.


--
-- TOC entry 2961 (class 0 OID 0)
-- Dependencies: 232
-- Name: customer_id_seq; Type: SEQUENCE SET; Schema: counter; Owner: postgres
--

SELECT pg_catalog.setval('counter.customer_id_seq', 3, true);


--
-- TOC entry 2962 (class 0 OID 0)
-- Dependencies: 236
-- Name: m_provider_id_seq; Type: SEQUENCE SET; Schema: counter; Owner: postgres
--

SELECT pg_catalog.setval('counter.m_provider_id_seq', 5, true);


--
-- TOC entry 2963 (class 0 OID 0)
-- Dependencies: 234
-- Name: package_id_seq; Type: SEQUENCE SET; Schema: counter; Owner: postgres
--

SELECT pg_catalog.setval('counter.package_id_seq', 2, true);


--
-- TOC entry 2964 (class 0 OID 0)
-- Dependencies: 228
-- Name: s_label_menu_id_seq; Type: SEQUENCE SET; Schema: counter; Owner: postgres
--

SELECT pg_catalog.setval('counter.s_label_menu_id_seq', 3, true);


--
-- TOC entry 2965 (class 0 OID 0)
-- Dependencies: 230
-- Name: s_menu_id_seq; Type: SEQUENCE SET; Schema: counter; Owner: postgres
--

SELECT pg_catalog.setval('counter.s_menu_id_seq', 6, true);


--
-- TOC entry 2966 (class 0 OID 0)
-- Dependencies: 240
-- Name: t_transaction_id_seq; Type: SEQUENCE SET; Schema: counter; Owner: postgres
--

SELECT pg_catalog.setval('counter.t_transaction_id_seq', 4, true);


--
-- TOC entry 2809 (class 2606 OID 32925)
-- Name: m_provider m_provider_pk; Type: CONSTRAINT; Schema: counter; Owner: postgres
--

ALTER TABLE ONLY counter.m_provider
    ADD CONSTRAINT m_provider_pk PRIMARY KEY (id);


--
-- TOC entry 2807 (class 2606 OID 32915)
-- Name: m_package package_pk; Type: CONSTRAINT; Schema: counter; Owner: postgres
--

ALTER TABLE ONLY counter.m_package
    ADD CONSTRAINT package_pk PRIMARY KEY (id);


--
-- TOC entry 2801 (class 2606 OID 32897)
-- Name: s_label_menu s_label_menu_pk; Type: CONSTRAINT; Schema: counter; Owner: postgres
--

ALTER TABLE ONLY counter.s_label_menu
    ADD CONSTRAINT s_label_menu_pk PRIMARY KEY (id);


--
-- TOC entry 2803 (class 2606 OID 32895)
-- Name: s_menu s_menu_pk; Type: CONSTRAINT; Schema: counter; Owner: postgres
--

ALTER TABLE ONLY counter.s_menu
    ADD CONSTRAINT s_menu_pk PRIMARY KEY (id);


--
-- TOC entry 2805 (class 2606 OID 32948)
-- Name: t_customer t_customer_pk; Type: CONSTRAINT; Schema: counter; Owner: postgres
--

ALTER TABLE ONLY counter.t_customer
    ADD CONSTRAINT t_customer_pk PRIMARY KEY (id);


--
-- TOC entry 2811 (class 2606 OID 32945)
-- Name: t_transaction t_transaction_pk; Type: CONSTRAINT; Schema: counter; Owner: postgres
--

ALTER TABLE ONLY counter.t_transaction
    ADD CONSTRAINT t_transaction_pk PRIMARY KEY (id);


-- Completed on 2020-07-15 09:11:42

--
-- PostgreSQL database dump complete
--

