--
-- PostgreSQL database dump
--

-- Dumped from database version 12.1
-- Dumped by pg_dump version 12.1

-- Started on 2020-07-15 09:12:34

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 4 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 2934 (class 0 OID 0)
-- Dependencies: 4
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 239 (class 1259 OID 32928)
-- Name: s_personal_data; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.s_personal_data (
    id integer NOT NULL,
    username character varying(50),
    name character varying(200),
    address character varying(200),
    create_at timestamp without time zone DEFAULT CURRENT_TIMESTAMP,
    create_by character varying(50),
    statusid smallint,
    photo text
);


ALTER TABLE public.s_personal_data OWNER TO postgres;

--
-- TOC entry 238 (class 1259 OID 32926)
-- Name: personal_data_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.personal_data_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personal_data_id_seq OWNER TO postgres;

--
-- TOC entry 2937 (class 0 OID 0)
-- Dependencies: 238
-- Name: personal_data_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.personal_data_id_seq OWNED BY public.s_personal_data.id;

--
-- TOC entry 2789 (class 2604 OID 32931)
-- Name: s_personal_data id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_personal_data ALTER COLUMN id SET DEFAULT nextval('public.personal_data_id_seq'::regclass);

--
-- TOC entry 2928 (class 0 OID 32928)
-- Dependencies: 239
-- Data for Name: s_personal_data; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.s_personal_data (id, username, name, address, create_at, create_by, statusid, photo) FROM stdin;
2	m.annafia.super	Mochammad Annafia Aktafian	Dusun Jatianom	2020-01-02 01:05:16	m.annafia	1	\N
3	annafia.super	Annafia	\N	2020-07-14 15:36:04.578243	\N	0	\N
5	annafia.super	Annafia	\N	2020-07-14 19:14:39.398486	\N	0	annafia.super.jpg
6	annafia.super	Annafia	\N	2020-07-14 19:21:05.951722	\N	0	annafia.super.jpg
7	annafia.super	Annafia	\N	2020-07-14 19:21:53.309395	\N	0	annafia.super.jpg
8	annafia.super	Annafia	\N	2020-07-14 19:22:38.441294	\N	0	annafia.super.jpg
9	annafia.super	Annafia	\N	2020-07-14 19:22:46.705768	\N	0	annafia.super.jpg
10	annafia.super	Annafia	\N	2020-07-14 19:22:53.819713	\N	0	annafia.super.jpg
11	annafia.super	Annafia	Pandaan	2020-07-14 19:37:24.675253	\N	0	annafia.super.jpg
12	annafia.super	Moch. Annafia O	Pandaan, Pasuruan, Jawa Timur	2020-07-14 19:38:02.081488	\N	0	annafia.super.jpg
13	annafia.super	Moch. Annafia O	Pandaan, Pasuruan, Jawa Timur	2020-07-14 19:38:17.933688	\N	0	annafia.super.jpg
14	annafia.super.sekalee	Moch. Annafia Oktafian	Pandaan, Pasuruan, Jawa Timur	2020-07-14 19:38:51.986107	\N	1	annafia.super.jpg
1	m.annafia	Mochammad Annafia Aktafian	Dusun Jatianom	2020-01-02 01:05:16	m.annafia	0	\N
15	m.annafia	Mochammad Annafia Aktafian	Dusun Jatianom	2020-07-14 20:48:20.758061	\N	1	m.annafia.jpg
\.

--
-- TOC entry 2940 (class 0 OID 0)
-- Dependencies: 238
-- Name: personal_data_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.personal_data_id_seq', 15, true);

--
-- TOC entry 2796 (class 2606 OID 32937)
-- Name: s_personal_data personal_data_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.s_personal_data
    ADD CONSTRAINT personal_data_pk PRIMARY KEY (id);


-- Completed on 2020-07-15 09:12:35

--
-- PostgreSQL database dump complete
--

