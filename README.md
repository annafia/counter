# Counter

# client

!--- Administrator Account --> <br>
username: m.annafia.super <br>
password: september09 <br>
encryption password: bcrypt <br>
authorization token: jwt <br>

!------------------- How to setup client --------------------------> <br>
Needs: <br>
1. Internet Connection
2. Web Server (I use apache)

Steps: <br>
1. Clone this project from Gitlab
2. Setup Web Server using alias to beautify the url

<pre>
    <code>
        Alias	/counter		C:\xampp5\htdocs\counter\client
        &lt;Directory &quot;C:\xampp5\htdocs\counter\client&quot;&gt; 
            Require all granted 

            RemoveOutputFilter js 
            RemoveOutputFilter css 

            RewriteEngine On 

            RewriteCond %{HTTP:Accept-Encoding} br 
            RewriteCond %{REQUEST_FILENAME}.br -f 
            RewriteRule ^(.*)$ $1.br [L] 
        &lt;/Directory&gt;
    </code>
</pre>

3. Don't forget to restart your web server after you make some changes

# master

!--- Administrator Account --> <br>
username: m.annafia.super <br>
password: september09 <br>
encryption password: bcrypt <br>
authorization token: jwt <br>

!------------------- How to setup this service --------------------------> <br>
!--- Needs: --> <br>
1. Node JS
2. Package Manager (such as NPM)
3. PM2
4. Internet Connection
5. Postgres
6. Proxy Pass

!--- Steps: --> <br>
1. Setup Postgres, There are 3 db schemas which is used in this apps that are authentication, public, & counter.
   You could find thats in folder named "database"
2. Clone this project from Gitlab
3. Install Package for NodeJS "npm i"
4. Setup Proxy Pass. In my case, I use apache as proxy pass, here is the code:
<pre>
    <code>
        &lt;Location &quot;/counter-api&quot;&gt;
            ProxyPass &quot;http://localhost:9600/counter-api&quot;
        &lt;/Location&gt;
    </code>
</pre>
5. Run this project from cli using "npm run dev"
6. Don't forget to restart your web server/proxy pass after you make some changes